class p02{
	
	static void fun(){

		System.out.println("If else in fun");
		String grade = "O";
		
		if(grade == "O"){
			
			System.out.println("Outstanding!");
		}else if(grade == "A"){
			
			System.out.println("Excellent!");
		}else if(grade == "B"){
			
			System.out.println("Can do better");
		}else if(grade == "C"){
			
			System.out.println("Need inprovement");
		}else if(grade == "D"){
			
			System.out.println("Need improvement, work harder!");
		}else if(grade == "E"){
			
			System.out.println("pass on border! need to improve a lot");
		}else{
		
			System.out.println("Fail!");
		}


	}

	public static void main(String[] args){
		
		System.out.println("switch: in main");

		char grade = 'A';

		switch (grade){
			
			case 'O':
				System.out.println("Outstanding!");
				break;

			case 'A':
				System.out.println("Excellent!");
				break;

			case 'B':
				System.out.println("Can do better");
				break;

			case 'C':
				System.out.println("Need inprovement");
				break;

			case 'D':
				System.out.println("Need improvement, work harder!");
				break;
		
			case 'E':
				System.out.println("pass on border! need to improve a lot");
				break;

			default:
				System.out.println("Fail!");
				break;

		}

		fun();

	}
}
