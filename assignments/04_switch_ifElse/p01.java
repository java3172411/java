/*Q1.
Write a program to check whether the given number is odd or even.
Input : 45
Output : 45 is an odd number.
Input : 28
Output : 28 is an even number.*/

class p01{

	static void fun(){
		
		System.out.println("IF else: in fun");

		int x = 45;
		if(x%2 == 1){
			
			System.out.println(x + " is an odd number");

		}else{
			
			System.out.println(x + " is an even number");

		}
	}
	
	public static void main(String[] args){
		
		System.out.println("Switch: in main");
		int x = 45;

		switch(x){
			
			case 1: if(x%2 == 1){
				
					System.out.println(x + " is an odd number");
					break;
			}

			case 2: if(x%2 == 0){
				
					System.out.println(x + " is an even number");
					break;
			}

			default: 
				System.out.println("invalid i/p!");
				break;
		}

		System.out.println();
		fun();


	}
}
