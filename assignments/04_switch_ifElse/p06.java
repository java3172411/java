class p06{

    static void fun(){

            System.out.println("If else: in fun");
            System.out.println("num = 0");
            int num = 0;

            if(num == 0){
                    System.out.println("Zero");
            }else if(num == 1){

                    System.out.println("One");
            }else if(num == 2){

                    System.out.println("Two");
            }else if(num == 3){

                    System.out.println("Three");
            }else if(num == 4){

                System.out.println("Four");
            }else if(num == 5){

                System.out.println("Five");
            }else{

                    System.out.println("Invalid input");
            }


    }
public static void main(String[] args){

            System.out.println("switch: in main");
            System.out.println("num = 4");

            int num = 4;

            switch (num){

                    case 0:
                            System.out.println("Zero");
                            break;

                    case 1:
                            System.out.println("One");
                            break;

                    case 2:
                            System.out.println("Two");
                            break;

                    case 3:
                            System.out.println("Three");
                            break;

                    case 4:
                            System.out.println("Four");
                            break;

                    case 5:
                            System.out.println("Five");
                            break;

                    default:
                            System.out.println("Invalid input");
                            break;

            }

            fun();

    }
}


