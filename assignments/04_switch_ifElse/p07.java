class p07{

    static void fun(){

            System.out.println("If else: in fun");
            System.out.println("amt = 15000");
            int amt = 15000;

            if(amt == 15000){
                    System.out.println("J & K");
            }else if(amt == 10000){

                    System.out.println("Manali");
            }else if(amt == 6000){

                    System.out.println("Amritsar");
            }else if(amt == 3){

                    System.out.println("Mahabaleshwar");
            }else{

                    System.out.println("Try next time");
            }


    }
public static void main(String[] args){

            System.out.println("switch: in main");
            System.out.println("amt = 2000");

            int amt = 2000;

            switch (amt){

                    case 15000:
                            System.out.println("J & K");
                            break;

                    case 10000:
                            System.out.println("Manali");
                            break;

                    case 6000:
                            System.out.println("Amritsar");
                            break;

                    case 2000:
                            System.out.println("Mahabaleshwar");
                            break;

                    default:
                            System.out.println("Try next time");
                            break;

            }

            fun();

    }
}


