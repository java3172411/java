class p03{

        static void fun(){

                System.out.println("If else in fun");
                String size = "XS";

                if(size == "XS"){

                        System.out.println("EXtra Small");
                }else if(size == "S"){

                        System.out.println("Small");
                }else if(size == "M"){

                        System.out.println("Medium");
                }else if(size == "L"){

                        System.out.println("Large");
                }else if(size == "XL"){

                        System.out.println("Xtra Large");
                }else{

                        System.out.println("Invalid input");
                }


        }
 public static void main(String[] args){

                System.out.println("switch: in main");

                String size = "XL";

                switch (size){

                        case "XS":
                                System.out.println("EXtra Small");
                                break;

                        case "S":
                                System.out.println("Small");
                                break;

                        case "M":
                                System.out.println("Medium");
                                break;

                        case "L":
                                System.out.println("Large");
                                break;

                        case "XL":
                                System.out.println("Xtra Large");
                                break;

                        default:
                                System.out.println("Invalid input");
                                break;

                }

                fun();

        }
}

