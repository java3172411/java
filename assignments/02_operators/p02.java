/*2.Write a program to compare two numbers using relational operators. Your program should: Compare whether the first number is greater than, less than, equal to, not equal to,greater than or equal to, and less than or equal to the second number. Display the comparison results.
Example:
first number: 7
second number: 5

7 > 5 : true
7 < 5 : false
7 == 5 : false
7 != 5 : true
7 >= 5 : true
7 <= 5 : false */

class p02{
	
	public static void main(String[] args){
		
		int num1 = 7;
		int num2 = 5;

		System.out.println("first number : "+num1+"\nsecond number : "+num2);
		System.out.println("\n7 > 5 : "+(num1>num2));
		System.out.println("\n7 < 5 : "+(num1<num2));
		System.out.println("\n7 == 5 : "+(num1==num2));
		System.out.println("\n7 != 5 : "+(num1!=num2));
		System.out.println("\n7 >= 5 : "+(num1>=num2));
		System.out.println("\n7 <= 5 : "+(num1<=num2));
	}
}
