class p08_LogicalCode{

	public static void main(String[] args){
		int x = 10;
		int y = 11;
		System.out.println((++x>=y)&&(x<++y));
		System.out.println(x);
		System.out.println(y);

	}
}

class p08_BitwiseOperator{

	public static void main(String[] args){

		int x = 15;
		int y = 35;

		System.out.println(x & y);
		System.out.println(x | y);
	}
}
