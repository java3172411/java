/* 4.Write a program to demonstrate logical operators. Your program should: Perform logical AND, logical OR, and logical NOT operations on the given boolean values. Display the results of each operation.
 Example:
 first boolean value (true/false): true
 second boolean value (true/false): false

 Logical AND: true && false = false
 Logical OR: true || false = true
 Logical NOT for the first value: !true = false
 Logical NOT for the second value: !false = true */

class p04{
	
	public static void main(String[] args){
		
		boolean num1 = true;
		boolean num2 = false;

		
		System.out.println("first boolean value : true");
		System.out.println("second boolean valuse : false");
		System.out.println("\nAND : "+(num1&&num2));
		System.out.println("\nOR: "+(num1||num2));
		System.out.println("\nNOT true : "+(!num1));
		System.out.println("\nNOT false : "+(!num2));
	}
}
