/*6.Write a program to demonstrate various assignment operators.Your program should: Initialize two variables with numeric values.Use compound assignment operators like +=, -=, *=, /=, %= to modify the values of the variables.Display the updated values after each operation.
Example:
Initial value of a: 10
Initial value of b: 5

After a += 3, a = 13
After b -= 2, b = 3
After a *= 2, a = 26
After b /= 3, b = 1
After a %= 5, a = 1 */

class p06{
	
	public static void main(String[] args){
	
		int a = 10;
		int b = 5;

		System.out.println("Initial value of a : "+a+"\nInitial value of b : "+b);
		System.out.println("\nAfter a += 3, a = " + (a+=3));
		System.out.println("\nAfter b -= 2, b = " + (b-=2));
		System.out.println("\nAfter a *= 2, a = " + (a*=2));
		System.out.println("\nAfter b /= 3, b = " + (b/=3));
		System.out.println("\nAfter a %= 5, a = " + (a%=5));
	}
}
