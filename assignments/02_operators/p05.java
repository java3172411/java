/*5.Write a program to perform bitwise operations on two integers. Your program should:Perform bitwise AND, bitwise OR, bitwise XOR, left shift, and right shift operations on the given integers.Display the results of each operation.
 * Example:
 *
 * Enter the first integer: 5
 * Enter the second integer: 3
 *
 * Bitwise AND: 5 & 3 = 1
 * Bitwise OR: 5 | 3 = 7
 * Bitwise XOR: 5 ^ 3 = 6
 * Left shift: 5 << 1 = 10
 * Right shift: 5 >> 1 = 2*/

class p05{
	
	public static void main(String[] args){
		
		int num1 = 5;
		int num2 = 3;

		
		System.out.println("first value : 5");
		System.out.println("second valuse : 3");
		System.out.println("\nBitwise AND : "+(num1&num2));
		System.out.println("\nBitwise OR: "+(num1|num2));
		System.out.println("\nBitwise XOR : "+(num1^num2));
		System.out.println("\nLeftshift : "+(num1<num2));
		System.out.println("\nRightshift : "+(num1>num2));
	}
}
