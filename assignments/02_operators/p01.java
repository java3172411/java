/*1.Write a program to perform basic arithmetic operations on two numbers.Your program should:Perform addition, subtraction, multiplication, and division on the given numbers.Display the results of each operation.
Example:
first number: 10
second number: 5

Addition: 10 + 5 = 15
Subtraction: 10 - 5 = 5
Multiplication: 10 * 5 = 50
Division: 10 / 5 = 2*/

class p01{
	
	public static void main(String[] args){
	
		int num1 = 10;
		int num2 = 5;

		int sum = num1+num2;
		int diff = num1-num2;
		int pro = num1*num2;
		int div = num1/num2;

		System.out.println("first number : "+num1+"\nsecond number : "+num2);
		System.out.println("\nAddition : 10 + 5  = "+sum);
		System.out.println("Subtraction : 10 + 5 = "+diff);
		System.out.println("Multiplication : 10 + 5 = "+pro);
		System.out.println("Division : 10 / 5 = "+div);
	}
}
