//4. Write a program to convert all even numbers into 0 and odd numbers into 1 in a given array.

import java.io.*;

class p04_even0Odd1{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int arr []= new int[size];
		
		for(int i=0; i<size; i++){
			
			System.out.println("Enter "+i+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				
				arr[i] = 0;
			}else{
				
				arr[i] = 1;
			}
		}

		for(int i=0; i<size; i++){
			
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
