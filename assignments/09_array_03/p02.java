//2. Write a program to find the first occurrences of a specific number in an array. Print the index of a first occurrence.

import java.io.*;

class p02_first_occurance{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader (System.in));
		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());
		
		int arr [] = new int[size];
		for(int i=0; i<size; i++){
			
			System.out.println("Enter "+i+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.print("Specific number : ");
		int x = Integer.parseInt(br.readLine());

		for(int i=0; i<size; i++){
			
			if(x == arr[i]){
				System.out.println("Num "+x+" first occurance at index : "+i);
				break;
			}
		}
		System.out.println("End Code");
	}
}
