//5. WAP to convert all negative numbers into their squares in a given array.

import java.io.*;

class p05_squares{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int arr []= new int[size];
		
		for(int i=0; i<size; i++){
			
			System.out.println("Enter "+i+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]<0){
				
				arr[i] = arr[i]*arr[i];
			}
		}

		for(int i=0; i<size; i++){
			
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
