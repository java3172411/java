//06. Write a program to print all the consonants in an array.

import java.io.*;

class p06_consonants{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		char ch[] = new char[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+i+" char : ");
			ch[i] = (char)br.read();
			br.skip(1);
		}

		System.out.println("Consonants in array : ");
		for(int i=0; i<size; i++){
			
			if(ch[i] != 'a' && ch[i] != 'A' && ch[i] != 'e' && ch[i] != 'E' && ch[i] != 'i' && ch[i] != 'I' && ch[i] != 'o' && ch[i] != 'O' && ch[i] != 'U' && ch[i] != 'u'){
				
				System.out.print(ch[i] + " ");
			}
		}
		System.out.println();
	}
}
