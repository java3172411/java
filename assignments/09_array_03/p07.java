//07. Check the size of the array and if array size is odd and greater than or equal to 5, then print the odd elements, else print the even numbers.

import java.io.*;


class p07_sizeOfArray{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0; i<size; i++){
			
			if(size%2==1 && size>=5){
				
				if(arr[i]%2==1){
					
					System.out.print(arr[i] + " ");
				}
			}else{
				if(arr[i]%2==0){
					
					System.out.print(arr[i] + " ");
				}				

			}
		}
		System.out.println();
	}
}
