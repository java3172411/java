//10. Write a program to print the product of prime elements in an array.

import java.io.*;

class p10_primeProduct{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		int cnt = 0;
		int pro = 1;

		for(int i=0; i<size; i++){
			
			for(int j=1; j<=arr[i]; j++){
								
				if(arr[i]%j == 0){
					
					cnt++;
				}
			}


			if(cnt==2){
				
				pro*=arr[i];
			}
			cnt=0;
		}
		
		System.out.println("Prime numbers in array : "+pro);
	}
}
