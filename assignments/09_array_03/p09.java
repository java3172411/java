// 09.Print the prime numbers in an array.

import java.io.*;

class p09_primeNum{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Prime numbers in array : ");
		int cnt = 0;

		for(int i=0; i<size; i++){
			
			for(int j=1; j<=arr[i]; j++){
								
				if(arr[i]%j == 0){
					
					cnt++;
				}
			}


			if(cnt==2){
				
				System.out.print(arr[i] + " ");
			}
			cnt=0;
		}
		System.out.println();
	}
}
