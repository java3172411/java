//1.Write a program to add 15 in all elements of the array and print it.

import java.io.*;

class p01_puls_15{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		for(int i=0; i<size; i++){
			
			System.out.println("Enter "+i+" element : ");
			arr[i] = Integer.parseInt(br.readLine()) + 15;
		}

		System.out.print("Array elements : ");
		for(int i=0; i<arr.length; i++){
			
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
