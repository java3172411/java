//3. WAP to find the number of occurrences of a specific number in an array. Print the count of occurrences.

import java.io.*;

class p03_multiple_occurances{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader (System.in));
		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());
		
		int arr [] = new int[size];
		for(int i=0; i<size; i++){
			
			System.out.println("Enter "+i+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.print("Specific number : ");
		int x = Integer.parseInt(br.readLine());
		int cnt = 0;

		for(int i=0; i<size; i++){
			
			if(x == arr[i]){
				cnt++;
			}
		}
		
		if(cnt==0){
			
			System.out.println("Num "+x+" not found in array");
		}else{
			
			System.out.println("Num "+x+" occured "+cnt+" times in array");
		}
	}
}
