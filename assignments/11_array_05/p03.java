//03 WAP to check if an array is a palindrome or not.

import java.io.*;

class p03_palindrome{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

        int flag =0;

        for(int i=0; i<size/2; i++){
            if(arr[i] != arr[size-1-i]){
                flag++;
            }
        }

        if(flag>0){
            System.out.println("The given array is not a palindrome.");
        }else{
            System.out.println("The given array is a palindrome.");
        }
    }
}