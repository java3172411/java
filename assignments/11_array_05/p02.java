//02.WAP to print the sum of odd and even numbers in an array.

import java.io.*;

class p02_evenOddSum{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

        int even_sum = 0;
        int odd_sum = 0;

        for(int i=0; i<size; i++){
			
			if(arr[i]%2==0){
                even_sum+=arr[i];
            }else{
                odd_sum+=arr[i];
            }
		}

        System.out.println("Odd Sum = "+odd_sum+"\nEven Sum = "+even_sum);
    }
}