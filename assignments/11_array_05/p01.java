//Q1. WAP to check whether the array is in ascending order or not.

import java.io.*;

class p01_asc{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

        int flag =0;

        for(int i=0; i<size-1; i++){
            if(arr[i] > arr[i+1]){
                flag++;
            }
        }

        if(flag>0){
            System.out.println("The given array is not in ascending Order.");
        }else{
            System.out.println("The given array is in ascending Order.");
        }
    }
}