//Q10. WAP to print the factorial of each element in an array.

import java.io.*;

class p10_factOfArr{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
        	}
		
		
		for(int i=0; i<size; i++){
			int fact=1;

			for(int j=1; j<=arr[i];j++ ){
				fact*=j;
			}

			arr[i]=fact;
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}
