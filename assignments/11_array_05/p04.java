//04. WAP to check the first duplicate element in an array and return its index.

import java.io.*;

class p04_firstDuplicate{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

        for(int i=0; i<size; i++){
            for(int j=i+1; j<size; j++){
                if(arr[i] == arr[j]){
                    System.out.println("First duplicate element present at index "+i);
                    break;
                }
            }            
        }
    }
}