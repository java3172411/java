//08. find the second minimum element in the array

import java.io.*;

class p08_secondMin{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : " );
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		for(int i=0; i<size; i++){
			
			System.out.println("Enter "+(i+1)+" element : ");	
			arr[i]=Integer.parseInt(br.readLine());
		}

		int min1=arr[0];
		for(int i=0; i<size; i++){
			
			if(arr[i]<min1){
				min1=arr[i];
			}
		}
		
		int min2;	
		if(min1 == arr[0]){
			
			min2=arr[1];
		}else{
			
			min2=arr[0];
		}

		for(int i=0; i<size; i++){
			
			if(arr[i]<min2 && min2>min1){
				
				min2=arr[i];
				System.out.println("Second minimum element in the array is : "+min2);
			}
		}
//		System.out.println("Second minimum element in the array is : "+min2);
		
	}
}
