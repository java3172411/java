//Q5. WAP to print the count of digits in elements of an array.

import java.io.*;

class p05_digit_cnt{
	
	public static void main(String [] args)throws IOException{
        
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

        for(int i=0; i<size; i++){

            int digit=0;
            int cnt=0;
            int num=arr[i];
            while(num!=0){
                digit=num%10;
                cnt++;
                num/=10;

                arr[i]=cnt;
            }
        }
            
        for(int x=0; x<size; x++){
            System.out.print(arr[x]+ " ");
        }
    }
}