//09. WAP to take a nummber from the user and store each element in an array by increasing the element by one.

import java.io.*;

class p09_numToArr{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter an integer number : ");
		int num = Integer.parseInt(br.readLine());
		int tmp = num;
		int digit=0;
		int cnt=0;

		while(tmp!=0){
		
			cnt++;
			tmp/=10;			
		}

		int arr[] = new int[cnt];
		System.out.println(cnt);

		int i = cnt-1;
		while(num!=0 && i>=0){
			digit = num%10;
			arr[i]=digit+1;
			num/=10;
			i--;
		}

		System.out.println("Array of num : ");
		for(int j=0; j<cnt; j++){
			
			System.out.println(arr[j] + " at index : " + j);
		}

				
	}
}
