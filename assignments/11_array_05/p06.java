//06 WAP to find the first prime number in an array.

import java.io.*;

class p06_firstPrime{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

	        int cnt=0;

        	for(int i=0; i<size; i++){
			for(int j=1; j<=arr[i]; j++){
	                	if(arr[i]%j==0){
		                    cnt++;
		                }
	        	}
	
        	       	if(cnt==2){
		                System.out.println("First prime number found at index "+i);
        		        break;
	            	}
	        	cnt=0;
        	}
    	}
}
