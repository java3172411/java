class p01{
    public static void main(String[] args) {
        long digit = 0L;
        long num = 2569185L;

        while (num!=0) {
            digit = num%10;
            if(digit%2 == 0){
                System.out.print(digit+ " ");
            }
            num/=10;

        }
    }
}