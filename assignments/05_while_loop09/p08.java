class p08{
    public static void main(String[] args) {
        long digit = 0L;
        long num =256985L;
        long pro  = 1;

        while (num!=0) {
            digit = num%10;
            if(digit%2 == 1){
                pro*=digit;
            }
            num/=10;
        }
        System.out.println(pro);
    }
}