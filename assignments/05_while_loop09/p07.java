class p07{
    public static void main(String[] args) {
        long digit = 0L;
        long num =216985L;
        int sum = 0;

        while (num!=0) {
            digit = num%10;
            if(digit%2 == 0){
                sum+=digit;
            }
            num/=10;
        }
        System.out.print(sum + " ");
    }
}