class p02{
    public static void main(String[] args) {
        long digit = 0L;
        long num = 2569185L;

        while (num!=0) {
            digit = num%10;
            if(digit%3 != 0){
                System.out.print(digit+ " ");
            }
            num/=10;

        }
    }
}