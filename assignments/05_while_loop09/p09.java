class p09{
    public static void main(String[] args) {
        long digit = 0L;
        long num =256985L;
        long pro  = 1;
        long sum = 0;

        while (num!=0) {
            digit = num%10;
            if(digit%2 == 1){
                sum=sum+(digit*digit);
            }
            num/=10;
        }
        System.out.println(sum);
    }
}