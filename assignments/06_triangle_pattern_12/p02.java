/*rows=4
a
$ $
a b c
$ $ $ $ */

import java.io.*;

class p02{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            char ch = 'a';
            for(int j=1; j<=i; j++){
                if(i%2 == 1){
                    System.out.print(ch++ + " ");
                }else{
                    System.out.print("$ ");
                }
                
            }
            System.out.println();
        }


    }
}