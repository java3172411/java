/*rows =3

4
4 a
4 b 6

rows =4
5
5 a
5 b 7
5 c 7 d */

import java.io.*;

class p09{
    public static void main(String[]args)throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        char ch = 'a';
        for(int i=1; i<=row; i++){
            int cnt = row+1;
            for(int j=1; j<=i; j++){
                if(j%2 == 0){
                    System.out.print(ch + " ");
                    ch++;
                }else{
                    System.out.print(cnt + " ");
                    cnt+=2;
                }            
            }
            System.out.println();
        }
    }
}
