/*rows=3
1
B C
1 2 3

rows=4
1
B C
1 2 3
G H I J */

import java.io.*;

class p06{
    public static void main(String[]args)throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        char ch = 'A';
        for(int i=1; i<=row; i++){
            for(int j=1; j<=i; j++){
                if(i%2 == 1){
                    System.out.print(j + " ");
                }else{
                    System.out.print(ch + " ");
                }   
                ch++;          
            }
            System.out.println();
        }
    }
}