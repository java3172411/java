/*rows=3
C
C B
C B A

rows=4
D
D C
D C B
D C B A */

import java.io.*;

class p03{
    public static void main(String[] args)throws IOException{
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            int ch = row;
            for(int j=1; j<=i; j++){
                
                System.out.print((char)(ch+64) + " ");
                ch--;
            }
            System.out.println();
        }
    }
}