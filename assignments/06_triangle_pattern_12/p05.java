/*rows =3
D
E F
G H I

rows = 4
E
F G
H I J
K L M N */

import java.io.*;

class p05{
    public static void main(String[]args)throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int num = row+1;
        for(int i=1; i<=row; i++){
            for(int j=1; j<=i; j++){
                    
                System.out.print((char)(num+64) + " ");
                num++;
            }
            System.out.println();
        }
    }
}