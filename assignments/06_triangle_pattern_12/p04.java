/*rows=3
c
C B
c b a
rows=4
d
D C
d c b
D C B A */

import java.io.*;

class p04{
    public static void main(String[]args)throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            int num = row;
            for(int j=1; j<=i; j++){
                if(i%2 == 1){
                    System.out.print((char)(num+96) + " ");
                }else{
                    System.out.print((char)(num+64) + " ");
                }
                num--;
            }
            System.out.println();
        }
    }
}