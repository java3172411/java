/*rows=3
1
1 c
1 e 3

rows=4
1
1 c
1 e 3
1 h 3 j */

import java.io.*;

class p08{
    public static void main(String[]args)throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        char ch = 'a';
        for(int i=1; i<=row; i++){
            int cnt = row+1;
            for(int j=1; j<=i; j++){
                if(j%2 == 1){
                    System.out.print(j + " ");
                }else{
                    System.out.print(ch + " ");
                }            
                ch++;
            }
            System.out.println();
        }
    }
}
