/*Rows = 4
2 4 6 8
10 12 14
16 18
20
Rows = 5
2   4   6   8    10
12  14  16  18
20  22  24
26  28
30 */

import java.io.*;

class p02{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int num = 2;

        for(int i=1; i<=row; i++){
            for(int j=row; j>=i; j--){
                System.out.print(num+ " ");
                num+=2;
            }
            System.out.println();
        }
    }
}
