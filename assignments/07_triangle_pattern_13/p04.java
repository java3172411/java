/*Rows = 3
F E D
C B
A
Rows = 4
J I H G
F E D
C B
A */

import java.io.*;

class p04{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        //int n = (row*(row+1))/2;        //guass theorem
        int sum=0;

        for(int i = 1; i<=row; i++){
            sum+=i;
        }

        for(int i=1; i<=row; i++){

            for(int j=row; j>=i; j--){

                System.out.print((char)(sum+64) + " ");
                sum--;                                
                
            }
            System.out.println();
        }
    }
}
