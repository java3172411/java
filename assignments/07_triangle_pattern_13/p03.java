/*Rows = 4
20 18 16 14
12 10 8
6 4
2
Rows = 5
30 28 26 24 22
20 18 16 14
12 10 8
6 4
2 */

import java.io.*;

class p03{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int num = row*(row+1);

        for(int i=1; i<=row; i++){
            for(int j=row; j>=i; j--){
                System.out.print(num+ " ");
                num-=2;
            }
            System.out.println();
        }
    }
}

