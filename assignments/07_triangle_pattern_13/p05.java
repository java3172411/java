/*Rows = 3
A B C
a b
A

Rows = 4
A B C D
a b c
A B
a */

import java.io.*;

class p05{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            int x = 1;
            for(int j=row; j>=i; j--){
                if(i%2==1){
                    System.out.print((char)(x+64)+ " ");
                }else{
                    System.out.print((char)(x+96)+ " ");
                }
                x++;
            }
            System.out.println();
        }
    }
}

