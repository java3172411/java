/*
4 c 2 a
3 b 1
2 a
1
Rows = 5
5 d 3 b 1
4 c 2 a
3 b 1
2 a
1 */

import java.io.*;

class p07{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            for(int j=row; j>=i; j--){
                if(j%2==0){
                    System.out.print(j+ " ");
                }else{
                    System.out.print((char)(j+96)+ " ");
                }

            }
            System.out.println();
        }
    }
}

