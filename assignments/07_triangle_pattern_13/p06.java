/*Rows = 3
1 a 2
1 a
1
Rows = 4
1 a 2 b
1 a 2
1 a
1 */

import java.io.*;

class p06{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int r = row;
        for(int i=1; i<=row; i++){
            int x = 1;
            int ch = 1;
            for(int j=1; j<=r; j++){
                if(j%2==1){
                    System.out.print(x+ " ");
                    x++;
                }else{
                    System.out.print((char)(ch+96)+ " ");
                    ch++;
                }
            }
            r--;
            System.out.println();
        }
    }
}

