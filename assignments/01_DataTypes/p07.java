//7. A person is trying to save the world data he wants to store the world’s population and also wants to store India’s population(Print the world’s and India’s population using appropriate data types).

class p07{
	
	public static void main(String[] args){
		
		float world = 7.8f;
		float india = 1.39f;

		System.out.println("world's population : "+world+" billion \nindia's population : "+india+" billion");
	}
}
