//9. Write a program to print the temperature of the Air Conditioner in degrees and also print the standard room temperature using appropriate data types.

class p09{
	
	public static void main(String[] args){
		
		int std = 25;
		int ac = 22;

		System.out.println("Std room temp. : "+std+"\nAC temp : "+ac);
	}
}
