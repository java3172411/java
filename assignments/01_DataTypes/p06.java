//6. A person is storing date, month and year in variables (Write a code to print the date, month, year, and also print the total seconds in a day, month and year.)

class p06{
	
	public static void main(String[] args){
		
		int date = 20;
		int month = 01;
		long year = 2024;

		long day_seconds = 86400;
		long month_seconds = 2592000;
		long year_seconds = 31536000;

		System.out.println(date+"/"+month+"/"+year+"\nseconds in day : "+day_seconds+"\nseconds in month : "+month_seconds+"\nseconds in year : "+year_seconds);
	}
}
