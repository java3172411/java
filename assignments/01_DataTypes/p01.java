//1.A student is in class 11th and there are 12 divisions of the class which data type is feasible to print the class and divisions. (Write a code to print the class and division)

class p01{
	
	public static void main(String[] args){
		
		int std = 11;
		char d1 = 'A';
		char d2 = 'B';
		char d3 = 'C';
		char d4 = 'D';
		char d5 = 'E';
		char d6 = 'F';
		char d7 = 'G';
		char d8 = 'H';
		char d9 = 'I';
		char d10 = 'J';
		char d11 = 'K';
		char d12 = 'L';

		System.out.println(std + " div 1 : " + d1);
		System.out.println(std + " div 2 : " + d2);
		System.out.println(std + " div 3 :  " + d3);
		System.out.println(std + " div 4 : " + d4);
		System.out.println(std + " div 5 : " + d5);
		System.out.println(std + " div 6 : " + d6);
		System.out.println(std + " div 7 : " + d7);
		System.out.println(std + " div 8 : " + d8);
		System.out.println(std + " div 9 : " + d9);
		System.out.println(std + " div 10 : " + d10);
		System.out.println(std + " div 11 : " + d11);
		System.out.println(std + " div 12 : " + d12);
	}
}
