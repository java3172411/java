//left diagonal sum for sq matrix
//right diagonal sum for sq matrix
//diagonal sum for sq matrix

import java.util.*;
class DiagonalSum{
    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of rows : ");
        int row = sc.nextInt();
        System.out.println("Enter number of columns : ");
        int col = sc.nextInt();

        int arr [][] = new int[row][col];

        System.out.println("Enter array elements : ");
        for(int r=0; r<row; r++){
            for(int c=0; c<col; c++){
                arr[r][c] = sc.nextInt();
            }
        }

        System.out.println("\nArray : ");
        for(int r=0; r<row; r++){
            for(int c=0; c<col; c++){
                System.out.print(arr[r][c] + "\t");
            }
            System.out.println();
        }
        
        int sum = 0, leftDiagonalSum = 0, rightDiagonalSum = 0, diagonalSum = 0;

        //sum
        for(int r=0; r<row; r++){
            for(int c=0; c<col; c++){
                sum+=arr[r][c];
            }
        }
        System.out.println("\nArray Sum = "+sum);

        //left diagonal sum
        for(int r=0; r<row; r++){
            for(int c=0; c<col; c++){
                if(r==c){
                    leftDiagonalSum+=arr[r][c];
                }
            }
        }
        System.out.println("Left Diagonal Sum = "+leftDiagonalSum);

        //right diagonal sum
        for(int x=0,r=0; x<row&&r<row; x++,r++){
            for(int y=col-1,c=0; y>=0&&c<col; y--,c++){
                if(x==r && y==c){
                    rightDiagonalSum+=arr[r][c];
                }                
            }
        }
        System.out.println("Right Diagonal Sum = "+rightDiagonalSum);

        //diagonal sum
        if(row%2==1 && col%2==1){
            diagonalSum = (leftDiagonalSum+rightDiagonalSum)-(arr[(int)row/2][(int)col/2]);
        }else{
            diagonalSum = leftDiagonalSum+rightDiagonalSum;
        }
        System.out.println("Diagonal Sum = "+diagonalSum);

    }
}