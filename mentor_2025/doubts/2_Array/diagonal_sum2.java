import java.util.*;

class DiagonalSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter size of square matrix: ");
        int n = sc.nextInt();

        int arr[][] = new int[n][n];

        System.out.println("Enter array elements: ");
        for (int r = 0; r < n; r++) {
            for (int c = 0; c < n; c++) {
                arr[r][c] = sc.nextInt();
            }
        }

        System.out.println("Array:");
        for (int r = 0; r < n; r++) {
            for (int c = 0; c < n; c++) {
                System.out.print(arr[r][c] + "\t");
            }
            System.out.println();
        }

        int sum = 0, leftDiagonalSum = 0, rightDiagonalSum = 0;

        // Optimized diagonal sum calculation
        for (int i = 0; i < n; i++) {
            leftDiagonalSum += arr[i][i];          // Left diagonal (Top-left to Bottom-right)
            rightDiagonalSum += arr[i][n - 1 - i]; // Right diagonal (Top-right to Bottom-left)
            for (int j = 0; j < n; j++) {
                sum += arr[i][j]; // Sum of all elements
            }
        }

        // Avoid double counting of center element in odd-sized matrix
        int diagonalSum = leftDiagonalSum + rightDiagonalSum;
        if (n % 2 == 1) {
            diagonalSum -= arr[n / 2][n / 2];
        }

        System.out.println("Array Sum = " + sum);
        System.out.println("Left Diagonal Sum = " + leftDiagonalSum);
        System.out.println("Right Diagonal Sum = " + rightDiagonalSum);
        System.out.println("Diagonal Sum = " + diagonalSum);
    }
}
