package IO;
import java.util.*;

class Demo{
	
	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		int x = sc.nextInt();

		if(x>10){
			
			System.out.println(">10");
		}else if(x<10){
			System.out.println("<10");
		}else if(x==10){
			System.out.println("=10");
		}else{
			
			System.out.println("not int");
		}
	}
}
