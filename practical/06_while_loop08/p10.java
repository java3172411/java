/*10.Write a program to print the sum of digits in the given number.
Input: 9307922405
Output: sum of digits in 9307922405 is 41 */

class p10{
    public static void main(String[] args){

        long digit = 0L;
        long num = 9307922405L;
        long temp = num;
        int sum=0;

        while(num!=0){
            digit = num%10;
            sum+=digit;
            num/=10;
        }
        System.out.println("sum of digits in "+temp+" is "+sum);
    }
}