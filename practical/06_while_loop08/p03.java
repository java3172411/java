/*Write a program to print the character sequence given below when the
range given is
Input : start = 1 and end =6.
This means iterate the loop from 1 to 6
Output: A B C D E F */

class p03{

    public static void main(String[] args) {
        
        int start = 1;
        int end = 6;

        while(start<=end){

            System.out.print((char)(start+64) +" ");
            start++;
        }
        System.out.println();
    }

}