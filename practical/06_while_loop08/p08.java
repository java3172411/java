/*Write a program to print the odd digits of a given number.
Input : 216985
Output : 5 9 1 */

class p08{
    public static void main(String[] args){
        int digit = 0;
        int num = 216985;

        while(num!=0){
            digit = num%10;
            if(digit%2 == 1){
                System.out.print(digit+ " ");
            }
            
            num/=10;
        }
    }
}