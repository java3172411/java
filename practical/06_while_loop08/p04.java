/*Write a program to print the character sequence given below where the input: start=1, end =6
This means iterate the loop from 1 to 6
Output: A 2 C 4 E 6 (if num is odd print the character) */

class p04{

    public static void main(String[] args) {
        
        int start = 1;
        int end = 6;

        while(start<=end){

            if(start%2==0){

                System.out.print(start + " ");

            }else{
                System.out.print((char)(start+64) +" ");
            }

            start++;
        }
        System.out.println();
    }

}