/*7. Write a program to count the digits in the given number.
Input: num = 93079224
Output: Count of digits= 8 */

class p07{
    public static void main(String[] args){
        int digit = 0;
        int num = 93079224;
        int cnt=0;

        while(num!=0){
            digit = num%10;
            cnt++;
            num/=10;
        }
        System.out.println("Count of digits= "+cnt);
    }
}