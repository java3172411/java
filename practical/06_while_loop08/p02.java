/*Write a program that prints the number whose square is an odd
number in the given range.
input: start = 150, end=198
output: 151 153 155 157 159 161 163 165 167 169 171 173 175 177 179
181 183 185 187 189 191 193 195 197 */

class p02{

    public static void main(String[] args) {
        
        int start = 150;
        int end = 198;

        while(start<=end){

            if((start*start)%2 == 1){
                System.out.print(start +" ");
            }            
            start++;
        }
    }

}