/*Row= 4
1 3 5 7
9 11 13 15
17 19 21 23
25 27 29 31 */

class p05{

    public static void main(String[] args){

        int row = 4;
        int num = 1;

        for(int i =1 ; i<=row; i++){
            for(int j = 1; j<=row; j++){
                System.out.print(num + " ");
                num+=2;
            }
            System.out.println();
        }

    }
}