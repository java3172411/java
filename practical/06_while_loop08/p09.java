/*9. Write a program to count the odd digits and even digits in the given
number.
Input: 214367689
Output: Odd count : 4
Even count : 5 */

class p09{
    public static void main(String[] args){
        int digit = 0;
        int num = 214367689;
        int ecnt = 0;
        int ocnt = 0;

        while(num!=0){
            digit = num%10;
            if(digit%2 == 1){
                ocnt++;
            }else{
                ecnt++;
            }
            num/=10;
        }

        System.out.println("Odd count : "+ocnt);
        System.out.println("Even count : "+ecnt);

    }
}