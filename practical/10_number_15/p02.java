/*Write a program to check whether the given number is prime or not.
Input: 7
Output: 7 is a prime number. */

import java.io.*;

class p02 {

    public static void main(String[] args)throws IOException {
        
        int num = 0;
        int cnt=0;
        System.out.print("Enter a number: ");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        num = Integer.parseInt(br.readLine());

        for(int i = 1; i<=num; i++){
            if(num%i==0){
                cnt++;
            }
        }
        
        if(cnt == 2){
            System.out.println(num + " is a prime number.");
        }else{
            System.out.println(num + " is not a prime number.");
        }

    }
    
}
