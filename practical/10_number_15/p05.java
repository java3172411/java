/*Write a program to reverse the given number.
Input : 7853
Output : Reverse of 7853 is 3587. */

import java.io.*;

class p05{
    public static void main(String[] args)throws IOException{

        int num = 0;
        int rev_num = 0;

        System.out.print("Enter a number: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        num = Integer.parseInt(br.readLine());
        int temp = num;

        while(temp!=0){
            rev_num = rev_num*10+(temp%10);
            temp/=10;
        }

        System.out.println("Reverse of "+num+" is "+rev_num);
       
    }
}