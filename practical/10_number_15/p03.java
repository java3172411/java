/*Write a program to check whether the given number is composite or
not.
Input : 10
Output : 10 is a composite number. */

import java.io.*;

class p03 {

    public static void main(String[] args)throws IOException {
        
        int num = 0;
        int cnt=0;
        System.out.print("Enter a number: ");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        num = Integer.parseInt(br.readLine());

        for(int i = 1; i<=num; i++){
            if(num%i==0){
                cnt++;
            }
        }
        
        if(cnt == 2){
            System.out.println(num + " is a not a composite number.");
        }else{
            System.out.println(num + " is a composite number.");
        }

    }
    
}
