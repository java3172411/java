/*Write a program to print the factors of a given number.
Input: 12
Output: Factors of 12 are 1,2,3,4,6,12 */

import java.io.*;

class p01 {

    public static void main(String[] args)throws IOException {
        
        int num = 0;
        System.out.print("Enter a number: ");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        num = Integer.parseInt(br.readLine());

        for(int i = 1; i<=num; i++){
            if(num%i==0){
                System.out.print(i+ " ");
            }
        }

    }
    
}
