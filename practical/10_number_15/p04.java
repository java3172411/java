/*Write a program to print the factorial of the number.
Input : 5
Output : Factorial of 5 is 120. */

import java.io.*;

class p04{
    public static void main(String[] args)throws IOException{

        int num = 0;
        int fact = 1;

        System.out.print("Enter a number: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        num = Integer.parseInt(br.readLine());

        for(int i =1; i<=num; i++){
            fact*=i;
        }

        System.out.println("Factorial of "+num+" is "+fact);
    }
}