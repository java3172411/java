/*Write a program to check whether the given number is palindrome or
not.
Input : 12121
Output : 12121 is a palindrome number.
Input : 12345
Output : 12345 is not a palindrome number.*/

import java.io.*;

class p06{
    public static void main(String[] args)throws IOException{

        int num = 0;
        int rev_num = 0;

        System.out.print("Enter a number: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        num = Integer.parseInt(br.readLine());
        int temp = num;

        while(temp!=0){
            rev_num = rev_num*10+(temp%10);
            temp/=10;
        }

        if(rev_num==num){
            System.out.println(num+" is a palindrome number");
        }else{
            System.out.println(num+" is not a palindrome number");
        }
       
       
    }
}