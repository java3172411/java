//6.Write a program to print the products of odd indexed elements, in an array. Where you have to take size input and elements input from the user 

import java.io.*;

class p06_proOfOddIndex{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int [] arr = new int[size];
		int pr = 1;

		System.out.println("Enter elements : ");
		for(int i=0; i<size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(i%2 == 1){
			
				pr*=arr[i];
			}
		}
		System.out.println("Product of odd indexed elements : "+pr);

	}
}
