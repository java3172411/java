//7.WAP to print the array , if the user given size of an array is even then print the alternate elements in an array, else print the whole array.

import java.io.*;

class p07_alternateOrWholeArr{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int arr [] = new int[size];


		System.out.print("Enter elements : ");
		for(int i=0; i<size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array elements are : ");
		for(int i=0; i<size; i++){
			
			if(size%2==0){
				
				if(i%2==0){
					
					System.out.println(arr[i]);
				}
			}else{
				System.out.println(arr[i]);

			}
		}
	}
}
