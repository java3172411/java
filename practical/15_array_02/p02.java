//WAP to print the sum of elements divisible by 3 in the array, where you have to take the size and elements from the user.

import java.io.*;

class p02_sumOfDivBy3{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());
		int sum = 0;
		
		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+i+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.print("Elements divisible by 3 : ");

		for(int i=0; i<arr.length; i++){
			
			if(arr[i] % 3 == 0){
				System.out.print(arr[i]);		
				sum+=arr[i];
			}
		}
		System.out.print("\nSum of elements divisible by 3 is: "+sum+"\n");
	}
}
