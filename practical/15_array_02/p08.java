//8.WAP to print the elements in an array which are greater than 5 but less than 9, where you have to take the size and elements from the user.

import java.io.*;

class p08_comparison{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int arr [] = new int[size];

		System.out.println("Enter elements : ");
		for(int i=0; i<size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0; i<size; i++){
			
			if(arr[i]>5 && arr[i]<9){
			
				System.out.println(arr[i]+" is greater than 5 but less than 9");
			}
		}
		System.out.println("No num greater than 5 and less than 9");
	}
}
