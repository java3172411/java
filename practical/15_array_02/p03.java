//3.WAP to check if there is any vowel in the array of characters if present then print its index, where you have to take the size and elements from the user.

import java.io.*;

class p03_vowels{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter size of array : ");
		int size = Integer.parseInt(br.readLine());

		char [] arr = new char[size];
		System.out.println("Enter elements : ");

		for(int i=0; i<size; i++){
			
			arr[i] = (char)br.read();
			br.skip(1);
		}

		for(int i=0; i<size; i++){
			
			if(arr[i] == 'a' || arr[i] == 'A' || arr[i] == 'e' || arr[i] == 'E' || arr[i] == 'i' || arr[i] == 'I' || arr[i] == 'o' || arr[i] == 'O'|| arr[i] == 'u' || arr[i] == 'U'){
				
				System.out.println("vowel " + arr[i] + " found at index "+i);
			}
		}

	}
}
