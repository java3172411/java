//10. WAP to print the Maximum element in the array.

import java.io.*;

class p10_max{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
		System.out.println("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements : ");
		for(int i=0; i<size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		int max = arr[0];
		for(int i=0; i<size; i++){
			
			if(max<arr[i]){
				
				max = arr[i];
				System.out.println("Maximum number in the array is found at pos "+i+" is "+arr[i]);
			}else{
				System.out.println("Maximum number in the array is found at pos "+i+" is "+arr[i]);	
			}
		}
		
	}
}
