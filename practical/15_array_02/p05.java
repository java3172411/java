//5.Write a program to print the sum of odd indexed elements, in an array. Where you have to take size input and elements input from the user 

import java.io.*;

class p05_sumOfOddIndex{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int [] arr = new int[size];
		int sum = 0;

		System.out.println("Enter elements : ");
		for(int i=0; i<size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(i%2 == 1){
			
				sum+=arr[i];
			}
		}
		System.out.println("Sum of odd indexed elements : "+sum);

	}
}
