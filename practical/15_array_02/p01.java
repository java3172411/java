//WAP to count the even numbers in an array where you have to take the size and elements from the user. And also print the even numbers too

import java.io.*;

class p01_even_count{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		for(int i = 0; i<size; i++){
			
			System.out.print("Enter "+i+" element in array : ");
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int cnt = 0;
		System.out.print("Even numbers : ");

		for(int i=0; i<arr.length; i++){
			
			if(arr[i]%2==0){
				
				System.out.print(arr[i] + " ");
				cnt++;	
			
			}
		}
		
		System.out.println();
		System.out.println("Count of even elements is : " + cnt);


	}
}
