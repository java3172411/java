//9. WAP to print the minimum element in the array, where you have to take the size and elements from the user.

import java.io.*;

class p09_min{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
		System.out.print("Enter size of array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements : ");
		for(int i=0; i<size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		int min = arr[0];
		for(int i=0;i<size;i++){
			
			if (arr[i]<min){
				
				min = arr[i];
			}
		}

		System.out.println("Minimun number inside the array is : "+min);
	}
}
