//4.WAP to search a specific element in an array and return its index. Ask the user to provide the number to search, also take size and elements input from the user.

import java.io.*;

class p04_arr_search{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int [] arr = new int[size];
		
		System.out.println("Enter elements : ");
		for(int i=0; i<size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the number to search in array : ");
		int x = Integer.parseInt(br.readLine());
		int cnt = 0;
		
		for(int i=0; i<size; i++){
			
			if(arr[i] == x){
				
				System.out.println(x+" found at index "+i);
				cnt++;
			}
		}
		if(cnt==0){
			
			System.out.println("Number not found :(");
		}

	}
}
