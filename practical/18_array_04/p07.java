//Q7. WAP to convert lowercase characters to UPPERCASE characters.(Take input from the user)

import java.io.*;

class p07_lowerUPPERCASE{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		char ch[] = new char[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+i+" char : ");
			ch[i] = (char)br.read();
			br.skip(1);
		}

        for(int i=0; i<size; i++){
			
			if('a'<=ch[i] && ch[i]<='z'){
				ch[i]-=32;				
			}
        }

        System.out.print("\n UPPERCASE array: ");
        for(int i=0; i<size; i++){
			System.out.print(ch[i] + " ");
		}

	System.out.println();
    }
}
