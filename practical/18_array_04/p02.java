//Q2.WAP to find the difference between minimum element in an array and maximum element in an array, take input from the user.

import java.io.*;

class p02_diff{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

        int min=arr[0];
		int max=arr[0];

        for(int i=1; i<size; i++){

            if(arr[i]>max){
                max = arr[i];
            }

            if(arr[i]<min){
                min = arr[i];
            }

        }

		int diff = max-min;

		System.out.println("The difference between the minimum and maximum elements is : "+diff);

	}
}
