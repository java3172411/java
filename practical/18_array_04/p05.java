//05. WAP to reverse the array(take input from the user

import java.io.*;

class p05_reverseArray{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		for(int i=0; i<size; i++){
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

	        System.out.print("input array: ");
        	for(int i=0; i<size; i++){
				System.out.print(arr[i] + " ");
			}

	        for(int i=0; i<size/2; i++){
        	    int temp = arr[i];
	            arr[i] = arr[size-1-i];
        	    arr[size-1-i] = temp;
	        }
	
        	System.out.print("\nreverse array: ");
	        for(int i=0; i<size; i++){
				System.out.print(arr[i] + " ");
		}
		System.out.println();
    }
}
