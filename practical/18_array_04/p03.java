//Q3. WAP to find the second largest element in an array.

import java.io.*;

class p03_secondMax{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

        int max1=arr[0];
        for(int i=0; i<size; i++){

            if(arr[i]>max1){
                max1 = arr[i];
            }
        }

        int max2=arr[0];
        for(int i=0; i<size; i++){
            if(arr[i]>max2 && arr[i]<max1){
                max2  = arr[i];
            }
            
        }

        System.out.println(max1);
        System.out.println("The second largest element in the array is: "+max2);

    }
}
