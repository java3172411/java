//Q10.Write a program to print the characters in an array which comes before the user given character.
import java.io.*;

class p10_printTillCh{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		char ch[] = new char[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+i+" char : ");
			ch[i] = (char)br.read();
			br.skip(1);
		}

		System.out.println("\nEnter character key: ");
        	char cha = (char)br.read();
		br.skip(1);

	        for(int i=0; ch[i]!=cha; i++){
			System.out.print(ch[i] + " ");
		}
	System.out.println();
	}
}

