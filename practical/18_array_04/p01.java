//WAP to take input from the user for size and elements of an array, where you have to print the average of array elements(Array should be of integers).

import java.io.*;

class p01_avg{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int sum=0;
		int avg=0;

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
			sum+=arr[i];
		}

		avg = sum/size;

		System.out.println("Avg of array elements is : "+avg);

	}
}
