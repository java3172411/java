//Q4. WAP to check whether the user given number occurs more than 2 times or equals 2 times.

import java.io.*;

class p04_occurance{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader( new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+(i+1)+" element : ");
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("\nEnter the number to check: ");
        int num = Integer.parseInt(br.readLine());
		int cnt=0;
		
        for(int i=0; i<size; i++){
			if(arr[i] == num){
                cnt++;
            }
		}

        if(cnt<2){
            System.out.println(num+" occurs less than 2 times in the array");
        }else if(cnt==2){
            System.out.println(num+" occurs exactly 2 times in the array");
        }else{
            System.out.println(num+" occurs more than 2 times in the array");
        }
	}
}

