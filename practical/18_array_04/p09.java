//Q9. WAP to replace the elements with #, which are not in the range of ‘a to z’.

import java.io.*;

class p09_charReplace{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		char ch[] = new char[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+i+" char : ");
			ch[i] = (char)br.read();
			br.skip(1);
		}

        for(int i=0; i<size; i++){
			
			if('a'<=ch[i] && ch[i]<='z'){
				continue;				
			}else{
                ch[i] = '#';
            }
        }

        System.out.print("\narray: ");
        for(int i=0; i<size; i++){
			System.out.print(ch[i] + " ");
		}
    }
}
