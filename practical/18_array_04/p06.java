//06. WAP to count the vowels and consonants in the given array(Take input from the user)

import java.io.*;

class p06_vowelConsonantCnt{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		char ch[] = new char[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+i+" char : ");
			ch[i] = (char)br.read();
			br.skip(1);
		}
		
		int vCnt = 0;
		int cCnt = 0;

		for(int i=0; i<size; i++){
			
			if(ch[i] != 'a' && ch[i] != 'A' && ch[i] != 'e' && ch[i] != 'E' && ch[i] != 'i' && ch[i] != 'I' && ch[i] != 'o' && ch[i] != 'O' && ch[i] != 'U' && ch[i] != 'u'){
				
				vCnt++;
			}else{
				cCnt++;
			}
		}
		System.out.println("Count of vowels: "+vCnt);
		System.out.println("Count of consonants: "+cCnt);
	}
}
