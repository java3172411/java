// 08.WAP to print the occurrence of a user given character.

import java.io.*;

class p08_chOccurance{
	
	public static void main(String [] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		char ch[] = new char[size];

		for(int i=0; i<size; i++){
			
			System.out.print("Enter "+i+" char : ");
			ch[i] = (char)br.read();
			br.skip(1);
		}

		System.out.println("\nEnter the character to check: ");
        char cha = (char)br.read();
        br.skip(1);
		int cnt=0;

        for(int i=0; i<size; i++){
			if(ch[i] == cha){
                cnt++;
            }
		}

        System.out.println(cha+" occurs "+cnt+" times in the array");

	}
}

