/*3 B 1 
C B A 
3 B 1*/

import java.io.*;

class p10{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number of rows : ");
        int rows = Integer.parseInt(br.readLine());

        
        for(int i = 1; i<=rows; i++){
            int ch = rows;
            for(int j = 1; j<=rows; j++){
                if(i%2==0){
                    System.out.print((char)(ch+64)+ " ");
                }else{
                    if(j%2==1){
                        System.out.print(ch+ " ");
                    }else{
                        System.out.print((char)(ch+64)+ " ");
                    }
                }
                ch--;
            }
            System.out.println();
        }
        
    }
}