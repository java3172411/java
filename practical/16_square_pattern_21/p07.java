/*A 4 A 
6 B 8  
C 10 C */

import java.io.*;

class p07{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number of rows : ");
        int rows = Integer.parseInt(br.readLine());

        int num = rows;
        for(int i = 1; i<=rows; i++){
            for(int j = 1; j<=rows; j++){
                if(num%2==1){
                    System.out.print((char)(i+64)+ " ");
                }else{
                    System.out.print(num+ " ");
                }
                num++;
            }
            System.out.println();
        }
        
    }
}