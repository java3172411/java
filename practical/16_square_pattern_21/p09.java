/*2 6 6 
3 4 9 
2 6 6  */

import java.io.*;

class p09{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number of rows : ");
        int rows = Integer.parseInt(br.readLine());

        
        for(int i = 1; i<=rows; i++){
            for(int j = 1; j<=rows; j++){
                if(i%2==1){
                    if(j%2==1){
                        System.out.print(j*2+ " ");
                    }else{
                        System.out.print(j*3+ " ");
                    }
                }else{
                    if(j%2==1){
                        System.out.print(j*3+ " ");
                    }else{
                        System.out.print(j*2+ " ");
                    }
                }
            }
            System.out.println();
        }
        
    }
}