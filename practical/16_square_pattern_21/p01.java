/*C B A 
3 3 3 
C B A  */

import java.io.*;

class p01{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number of rows : ");
        int rows = Integer.parseInt(br.readLine());

        for(int i = 1; i<=rows; i++){
            int n = 0;
            for(int j = 1; j<=rows; j++){
                if(i%2==1){
                    System.out.print((char)(rows+64-n)+ " ");
                    n++;
                }else{
                    System.out.print(rows+ " ");
                }
            }
            System.out.println();
        }
        
    }
}