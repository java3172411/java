/*9 4 5    
18 7 8   
27 10 11 */

import java.io.*;

class p03{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number of rows : ");
        int rows = Integer.parseInt(br.readLine());

        int num = rows;
        for(int i = 1; i<=rows; i++){
            for(int j = 1; j<=rows; j++){
                if(num%rows==0){
                    System.out.print(num*3+ " ");
                }else{
                    System.out.print(num+ " ");
                }
                num++;
            }
            System.out.println();
        }
        
    }
}