/*9 4 25   
18 7 8   
27 50 11 */

import java.io.*;

class p02{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number of rows : ");
        int rows = Integer.parseInt(br.readLine());

        int num = rows;
        for(int i = 1; i<=rows; i++){
            for(int j = 1; j<=rows; j++){
                if(num%3==0){
                    System.out.print(num*3+ " ");
                }else if(num%5==0){
                    System.out.print(num*5+ " ");
                }else{
                    System.out.print(num+ " ");
                }
                num++;
            }
            System.out.println();
        }
        
    }
}