/*9 4 25    
6 49 8    
81 10 121*/

import java.io.*;

class p06{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter number of rows : ");
        int rows = Integer.parseInt(br.readLine());

        int num = rows;
        for(int i = 1; i<=rows; i++){
            for(int j = 1; j<=rows; j++){
                if(num%2==1){
                    System.out.print((num*num)+ " ");
                }else{
                    System.out.print(num+ " ");
                }
                num++;
            }
            System.out.println();
        }
        
    }
}