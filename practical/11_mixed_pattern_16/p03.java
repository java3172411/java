/*Number of rows = 3
C B A
1 2 3
C B A
Number of rows = 4
D C B A
1 2 3 4
D C B A
1 2 3 4 */

import java.io.*;

class p03{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        
        for(int i=1; i<=row; i++){
            int num=1;
            for(int j=row; j>=1; j--){
                if(i%2==1){
                    System.out.print((char)(j+64)+ " ");
                }else{
                    System.out.print(num+ " ");
                }
                num++;
            }
            System.out.println();
        }
    }
}