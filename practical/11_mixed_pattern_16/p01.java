/*Number of rows = 4
1 2 3 4
5 6 7 8
9 10 11 12
13 14 15 16*/

import java.io.*;

class p01{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int num=1;
        for(int i=1; i<=row; i++){
            for(int j=1; j<=row; j++){
                System.out.print(num+ " ");
                num++;
            }
            System.out.println();
        }
    }
}
