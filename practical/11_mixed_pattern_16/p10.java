/*Write a program to print the square of odd digits from the given
number. (First reverse this number and then perform the operation)
Input : 45632985632
Output : 25, 9, 81, 25, 9 */

import java.io.*;

class p10{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        //int rev = 0;
        int rev = 0;
        
        while(num!=0){
            int digit = num%10;
            rev = (rev*10)+digit;
            num/=10;
        }

        while(rev!=0){
            int digit = rev%10;
            if(digit%2 == 1){
                System.out.print(digit*digit + " ");
            }
            rev/=10;
        }
        System.out.println();
    }
}