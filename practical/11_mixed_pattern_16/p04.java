/*Row = 3
3
2 4
1 2 3
Rows = 4
4
3 6
2 4 6
1 2 3 4 */

import java.io.*;

class p04{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int y = row;
        for(int i=1; i<=row; i++){
            int x = y;
            for(int j=1; j<=i; j++){
                    
                System.out.print(x+ " ");
                x+=x;
            }
            y--;
            System.out.println();
        }
    }
}