/*Number of rows = 3
C3 C2 C1
C4 C3 C2
C5 C4 C3*/

import java.io.*;

class p02{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        
        for(int i=1; i<=row; i++){
            int num=i+2;
            for(int j=1; j<=row; j++){
                System.out.print((char)(row+64));
                System.out.print(num+ " ");
                num--;
            }
            System.out.println();
        }
    }
}