/*Rows = 4
1 2 3 4
C B A
1 2
A
Rows = 5
1 2 3 4 5
D C B A
1 2 3
B A
1 */

import java.io.*;

class p09{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int x = row;
        for(int i=1; i<=row; i++){
            int num = 1;
            int x2 = x;
            for(int j=row; j>=i; j--){
                                    
                if(i%2==1){
                    System.out.print(num+ " ");
                    num++;
                }else{
                    System.out.print((char)((x2)+64)+ " ");
                    x2--;
                }
            }
            x--;
            System.out.println();
        }
    }
}