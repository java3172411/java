/*Rows = 3
c
3 2
c b a

Rows = 4
d
4 3
d c b
4 3 2 1 */

import java.io.*;

class p06{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        
        for(int i=1; i<=row; i++){
            int x = row;
            for(int j=1; j<=i; j++){
                if(i%2==1){
                    System.out.print((char)(x+96)+ " ");
                }else{
                    System.out.print(x+ " ");
                }
                x--;                
            }
            System.out.println();
        }
    }
}