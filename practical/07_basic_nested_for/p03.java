/*Number of rows = 3
1 1 1
2 2 2
3 3 3 */

class p03{
    public static void main(String[] args) {
        
        int row=3;
        for(int i = 1; i<= row; i++){
            for(int j = 1; j<= row; j++){
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}