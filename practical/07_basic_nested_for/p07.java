/*Number of rows = 4
d c b a
d c b a
d c b a
d c b a */

class p07{
    public static void main(String[] args) {
        
        int row=4;
    
        for(int i = 1; i<= row; i++){
            for(int j = row; j>= 1; j--){
                System.out.print((char)(j+96)+ " ");
            }
            System.out.println();
        }
    }
}