/*Number of rows = 3
1A 2B 3C
1A 2B 3C
1A 2B 3C */

class p06{
    public static void main(String[] args) {
        
        int row=3;
        for(int i = 1; i<= row; i++){
            for(int j = 1; j<= row; j++){
                System.out.print(j);
                System.out.print((char)(j+64)+ " ");
            }
            System.out.println();
        }
    }
}