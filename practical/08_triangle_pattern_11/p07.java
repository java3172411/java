/*Rows = 4
1 2 3 4
1 2 3
1 2
1
Rows = 3

1 2 3
1 2
1 */

import java.io.*;

class p07{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        
        for(int i=1; i<=row; i++){
            int x = 1;
            for(int j=row; j>=i; j--){
                System.out.print(x++ + " ");
            }
            System.out.println();
        }


    }
}