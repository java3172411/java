/*Row = 3
3
3 6
3 6 9
Rows = 4
4
4 8
4 8 12
4 8 12 16 */

import java.io.*;

class p04{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            for(int j=1; j<=i; j++){
                System.out.print(row*j + " ");
            }
            System.out.println();
        }


    }
}