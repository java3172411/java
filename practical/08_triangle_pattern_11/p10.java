/*rows = 4
65 B 67 D
B 67 D
67 D
D
rows = 3
A 66 C
66 C
C */

import java.io.*;

class p10{
    public static void main(String[] args) throws IOException{

        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int num = 65;

        for(int i=1; i<=row; i++){
            int num2 = num;
            for(int j=1; j<=row-i+1; j++){
                if(((row-(i+j))%2!=0)){
                   System.out.print((char)(num2)+ " ");
                }else{
                    System.out.print(num2+ " "); 
                }
                num2++;
            }
            num++;
            System.out.println();
        }
    }
}
