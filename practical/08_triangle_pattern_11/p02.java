/*Rows = 4
4
4 3
4 3 2
4 3 2 1 */

import java.io.*;

class p02{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            int num = row;
            for(int j=1; j<=i; j++){
                System.out.print(num-- + " ");
            }
            System.out.println();
        }


    }
}