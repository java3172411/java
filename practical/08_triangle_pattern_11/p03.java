/*
A
B C
C D E
D E F G */

import java.io.*;

class p03{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());

        for(int i=1; i<=row; i++){
            for(int j=1; j<=i; j++){
                System.out.print((char)(j+64) + " ");
            }
            System.out.println();
        }


    }
}