/*Rows = 3
3 3 3
2 2
1
Rows = 4
4 4 4 4
3 3 3
2 2
1 */

import java.io.*;

class p06{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int x = row;
        for(int i=1; i<=row; i++){
            for(int j=row; j>=i; j--){
                System.out.print(x + " ");
            }
            x--;
            System.out.println();
        }


    }
}