/*Rows = 4
D C B A
D C B
D C
D

Rows = 5
E D C B A
E D C B
E D C
E D
E */

import java.io.*;

class p09{
    public static void main(String[] args) throws IOException{
        
        System.out.print("Enter number of rows: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        
        for(int i=1; i<=row; i++){
            //int x = row;
            for(int j=row; j>=i; j--){
                System.out.print((char)(j+64) + " ");
            }
//            x--;
            System.out.println();
        }


    }
}
