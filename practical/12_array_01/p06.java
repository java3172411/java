/*Write a program where you have to take input from the user for a character array and
print the characters. */

import java.io.*;

class p06{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter array size : ");
        int size = Integer.parseInt(br.readLine());

        char arr[] = new char[size];

        for(int i =0; i<arr.length; i++){
            
            System.out.print("Enter array element "+(i+1)+" : ");
            arr[i] = (char)br.read();
	    br.skip(1);
                      
        }

        System.out.println("Elements in array: ");
        for(int i =0; i<size; i++){

            System.out.print(arr[i]+ " ");
 
        }
    }
}
