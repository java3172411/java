/*Write a program to print the sum of odd elements in an array.Take input from the user. */

import java.io.*;

class p04{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter array size : ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        for(int i =0; i<arr.length; i++){
            
            System.out.print("Enter array element "+(i+1)+" : ");
            arr[i] = Integer.parseInt(br.readLine());
                      
        }

        int sum = 0;

        for(int i =0; i<size; i++){

            if(arr[i]%2==1){
                sum+=arr[i];
            }
        }

        System.out.print("Sum of odd elements in the array is: "+sum);
    }
}