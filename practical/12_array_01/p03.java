/*3. Write a program to print the even elements in the array. Take input from the user. */

import java.io.*;

class p03{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter array size : ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        for(int i =0; i<arr.length; i++){
            
            System.out.print("Enter array element "+(i+1)+" : ");
            arr[i] = Integer.parseInt(br.readLine());
                      
        }

        System.out.println("Even elements in the array are : ");
        for(int i =0; i<size; i++){

            if(arr[i]%2==0){
                System.out.print(arr[i]+ " ");
            }
        }
    }
}