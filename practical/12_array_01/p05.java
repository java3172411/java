/*Write a program where you have to print the elements from the array which are less than 10.Take input from the user. */

import java.io.*;

class p05{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter array size : ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        for(int i =0; i<arr.length; i++){
            
            System.out.print("Enter array element "+(i+1)+" : ");
            arr[i] = Integer.parseInt(br.readLine());
                      
        }

        System.out.println("Elements less than 10 are: ");
        for(int i =0; i<size; i++){

            if(arr[i]<10){
                System.out.print(arr[i]+ " ");
            }
        }
    }
}