/*8. Write a program where you have to store the employee’s age working at a company, take count of employees from the user.Take input from the user. */

import java.io.*;

class p08{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter Employee count : ");
        int empCnt = Integer.parseInt(br.readLine());

        int arr[] = new int[empCnt];

        for(int i =0; i<arr.length; i++){
            
            System.out.print("Enter age of "+(i+1)+" employee : ");
            arr[i] = Integer.parseInt(br.readLine());
                      
        }

        for(int i =0; i<empCnt; i++){

            System.out.print(arr[i]+ " ");
 
        }
    }
}