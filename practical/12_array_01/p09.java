/*Write a program where you have to print the odd indexed elements. Take input from
the user */

import java.io.*;

class p09{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter array size : ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        for(int i =0; i<arr.length; i++){
            
            System.out.print("Enter array element "+(i+1)+" : ");
            arr[i] = Integer.parseInt(br.readLine());
                      
        }

        for(int i =0; i<size; i++){

            if(i%2==1){
                System.out.println(arr[i]+ " is an odd indexed element");
            }
        }
    }
}