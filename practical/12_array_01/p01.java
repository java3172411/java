/*Write a program to print the array with minimum 10 elements data.
 Output :
10, 20, 30, 40, 50, 60, 70, 80, 90, 100  */

class p01{
    public static void main(String[] args) {
        
        int arr[] = new int[10];

        int x = 10;
        for(int i =0; i<10; i++){

            arr[i] = x;
            System.out.print(arr[i]+ " ");
            x+=10;
        }
    }
}