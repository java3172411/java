/*7. Write a program to print the elements of the array which is divisible by 4. Take input
from the user. */

import java.io.*;

class p07{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter array size : ");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        for(int i=0; i<arr.length; i++){
            
            System.out.print("Enter array element "+(i+1)+" : ");
            arr[i] = Integer.parseInt(br.readLine());
                      
        }

        for(int i=0; i<size; i++){

            if(arr[i]%4==0){
                System.out.println(arr[i]+ " is divisible by 4 and its index is " +i);
            }
        }
    }
}