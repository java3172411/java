/*10. Write a real-time example where you have to use the array. Take input from the user. */

import java.io.*;

class p10{
    public static void main(String[] args)throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
        
        System.out.print("Enter array size : ");
        int size = Integer.parseInt(br.readLine());

        String arr[] = new String[size];

        System.out.println("Enter the names of the programming languages you know :");

        for(int i =0; i<arr.length; i++){
            
            System.out.print("Language "+(i+1)+" : ");
            arr[i] = br.readLine();
                      
        }

        System.out.println("\nYour programming languages are :");
        for(int i =0; i<size; i++){

            System.out.println((i+1)+". "+arr[i]);
 
        }
    }
}