/*4. Write a program to check whether the given Character is in UPPERCASE (Capital) or in lowercase. (take hardcoded values)
Input: ch = ‘a’;
Output: a is a lowercase character

Input: ch = ‘A’;
Output: A is an UPPERCASE character*/
class p04{
	
	public static void main(String[] args){
		
		char ch = 'j';

		if(ch>='a' && ch<='z'){
			
			System.out.println("ch is lower case");

		}else if(ch>='A' && ch<='Z'){
		
			System.out.println("ch is upper case");
		}else{
			
			System.out.println("invalid input");
		}
	}
}
