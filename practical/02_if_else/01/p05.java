/*5. Write a program to check whether the given number is divisible by 7 or not.
Input : 105
Output: Divisible by 7

Input: -31
Output: Not divisible by 7*/
class p05{
	
	public static void main(String[] args){
		
		int num = 49;

		if(num%7 == 0){
			
			System.out.println("number is divisible by 7");
		}else{
			
			System.out.println("number is not divisible by 7");
		}
	}
}
