class p08{
	
	public static void main(String[] args){
		
		float per = 75.37f;

		if(per>0){
			
			if(per >= 75.00){
				
				System.out.println("first class with distinction");

			}else if(per<75.00 && per>=60.00){
				
				System.out.println("first class");

			}else if(per<60.00 && per>=54.00){
				
				System.out.println("second class");

			}else if(per<54.00 && per>=47.00){
				
				System.out.println("pass");
			}else{
				
				System.out.println("better luck next time");
			}

		}else{
			
			System.out.println("Invalid input");
		}
	}
}
