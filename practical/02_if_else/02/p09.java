class p09{
	
	public static void main(String[] args){
		
		int a = 3;
		int b = 4;
		int c = 5;

		if(a>0 && b>0 && c>0){
			
			if(a*a + b*b == c*c || b*b + c*c == a*a || c*c + a*a == b*b){

				System.out.println("Its a pythagorean triplet");
			}else{
				System.out.println("Its not a pythagorean triplet");
			}

		}else{
			
			System.out.println("Invaild input");
		}
	}
}
