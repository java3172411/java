class p02{
	
	public static void main(String[] args){
		
		int num = 130;

		if(num%13 == 0){
			
			System.out.println(num + " is in the table of 13");
		}else{
			
			System.out.println(num + " is not in the table of 13");
		}
	}
}
