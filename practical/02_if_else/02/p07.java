class p07{
	
	public static void main(String[] args){
		
		double sellingPrice = 1300.54;
		double costPrice = 1200.32;

		if(sellingPrice >= 0 && costPrice >= 0){
			
			if(sellingPrice > costPrice){
				
				System.out.println("Profit of " + (sellingPrice-costPrice));

			}else if(costPrice > sellingPrice){
				
				System.out.println("Loss of " + (costPrice-sellingPrice));
			}else{
				
				System.out.println("Neither profit nor loss");
			}

		}else{
			
			System.out.println("one of the costs is innvalid");
		}
	}
}
