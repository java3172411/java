class p01{
	
	public static void main(String[] args){
		
		int num = 13;

		if(num<=1000 && num>=1){
			
			System.out.println(num + " is in range 1 to 1000");
		}else{
			
			System.out.println(num + " is not in range 1 to 1000");
		}
	} 
}
