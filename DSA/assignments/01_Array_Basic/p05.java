/*
 * 5] Replace all 0's with 5
    You are given an integer N. You need to convert all zeros of N to 5.
 
 * Example 1:
    Input:
    N = 1004
    Output: 1554
 * Explanation: There are two zeroes in 1004. 
   On replacing all zeroes with "5", the new number will be "1554".

 * Example 2:
    Input:
    N = 121
    Output: 121
 * Explanation: Since there are no zeroes in "121", the number remains as "121".

 * Expected Time Complexity: O(K) where K is the number of digits in N
 * Expected Auxiliary Space: O(1)
 * Constraints:
    1 <= N <= 10000
 */

import java.util.*;

class p05_Replace0with5{
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);

      System.out.println("Enter a number : ");
      int n = sc.nextInt();
      int x = n, r = 0, d = 0, ans = 0;

      while(x != 0){
         d = x%10;
         if(d == 0){
            r = r*10+5;
         }else{
            r = r*10+d;
         }
         x/=10;
      }

      while(r!=0){
         d=r%10;
         ans = ans*10+d;
         r/=10;
      }

      System.out.println(ans);

   }
}