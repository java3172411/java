/*
 * 9] Remove an Element at Specific Index from an Array
    Given an array of a fixed length. The task is to remove an element at a specific
    index from the array.

 * Example 1:
    Input:
    arr[] = { 1, 2, 3, 4, 5 }, index = 2
    Output: arr[] = { 1, 2, 4, 5 }

 * Example 2:
    Input:
    arr[] = { 4, 5, 9, 8, 1 }, index = 3
    Output: arr[] = { 4, 5, 9, 1 }

 */
