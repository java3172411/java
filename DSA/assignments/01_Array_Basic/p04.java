/*
 * 4] Product of array elements
    This is a functional problem. Your task is to return the product of array elements under a given modulo. The modulo operation finds the remainder after the division of one number by another. For example, K(mod(m)) = K % m = remainder obtained when K is divided by m.

 * Example 1:
    Input:
    1
    4
    1 2 3 4
    Output:
    24

 * Input:
    The first line of input contains T denoting the number of test cases. 
    Then each of the T lines contains a single positive integer N denoting 
    the number of elements in the array. The next line contains 'N' integer 
    elements of the array.

 * Output:
    Return the product of array elements under a given modulo.
    That is, return (Array[0] * Array[1] * Array[2] ... * Array[N-1]) % modulo.

 * Constraints:
    1 <= T <= 200
    1 <= N <= 10^5
    1 <= ar[i] <= 10^5

 * We use 10^9+7 as the modulo because it is a large prime number, ensuring correct modular arithmetic, and it fits within a 32-bit integer to prevent overflow. It is widely used in competitive programming due to its efficiency and mathematical properties.
 
 * The variable T represents the number of test cases, meaning it tells the program how many different arrays (or sets of numbers) you need to process.
    Each test case is treated as a separate instance, so you compute the product modulo for one array and then move on to the next array.
 */

import java.util.*;

class p04_ProductEle{
	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number of testcases : ");
		int t = sc.nextInt();

		for(int i=0; i<t; i++){
			System.out.println("Enter the number of elements : ");
			int n = sc.nextInt();
			int arr[] = new int[n];
			
			//Define the modulus value (10^9 + 7)
			final int MOD = 1000000007;
			int p = 1;
			
			for(int j=0; j<n; j++){
				arr[j] = sc.nextInt();
				p = ((p*arr[j])%MOD);	
			}

			System.out.println(p);
		}
	}
}
