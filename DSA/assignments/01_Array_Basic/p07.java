/*
 * 7] Form largest number from digits
    Given an array of numbers from 0 to 9 of size N. Your task is to rearrange elements
    of the array such that after combining all the elements of the array, the number formed is maximum.

 * Example 1:
    Input:
    N = 5
    A[] = {9, 0, 1, 3, 0}
    Output: 93100
 * Explanation: Largest number is 93100 which can be formed from array digits.

 * Example 2:
    Input:
    N = 3
    A[] = {1, 2, 3}
    Output: 321

 * Expected Time Complexity: O(N)
 * Expected Auxiliary Space: O(N)
 * Constraints:
    1 <= N <= 10^7
    0 <= Ai <= 9
 */

 import java.util.*;

class p07_FormMaxNum{
	
	public static void main(String [] a){
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number of elements : ");
		int n = sc.nextInt();

		System.out.println("Enter elements : ");
		for(int i=0; i<n; i++){
			
			al.add(sc.nextInt());
		}

		Collections.sort(al, Collections.reverseOrder());

		int maxNum = 0;
		for(Integer x : al){
			
			maxNum = maxNum*10+x;
		}

		System.out.println(maxNum);

      //T.C. : O(NlogN) due to sorting

	}
}