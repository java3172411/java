//wrong code
import java.util.ArrayList;

class MaxSubarray{
	
	public static void main(String [] args){
		
		int arr[] = new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4};
		int sum = 0;
		int maxSum = Integer.MIN_VALUE;
		ArrayList<Integer> subarray = new ArrayList<>();

		for(int i=0; i<arr.length; i++){
			
			sum = sum + arr[i];

			if(sum > maxSum){
				maxSum = sum;
				subarray.add(arr[i]);
			}

			if( (sum-arr[i]) == maxSum && (sum-arr[i]) < sum)	//if previous maxsum is equal to current maxsum && sum only increases
				subarray.add(arr[i]);

			if(sum<0){
				sum = 0;
				subarray.clear();
			}

			System.out.println(subarray);

			
		}
	}
}
