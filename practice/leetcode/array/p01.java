/*
* Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target. 
* You may assume that each input would have exactly one solution, and you may not use the same element twice.
* You can return the answer in any order.

Input: nums = [3,2,4], target = 6
Output: [1,2]

Input: nums = [3,3], target = 6
Output: [0,1]
*/

import java.util.Arrays;

class p01_twoSum{
	
	public static void main(String [] args){
		
		int arr1[] = new int[]{3,2,4};
		int arr2[] = new int[]{3,3};

		Solution obj = new Solution();
		int[] ans1 = obj.twoSum(arr1, 6);
		System.out.println(Arrays.toString(ans1));

		int[] ans2 = obj.twoSum(arr2, 6);
		System.out.println(Arrays.toString(ans2));

	}
}

class Solution{

	public int[] twoSum(int[] nums, int target) {
        
		int arr[] = new int[2];

		for(int i=0;i<nums.length;i++){
			for(int j=1;j<nums.length;j++){
				System.out.println("length : "+nums.length);
				System.out.println("i : "+i+"  nums[i] : "+nums[i]);
				System.out.println("j : "+j+"  nums[j] : "+nums[j]);
				System.out.println("sum : "+(nums[i]+nums[j]));
				if(nums[i]+nums[j]==target){
					System.out.println("inside if");
					arr[0] = i;
					System.out.println(arr[0]);
					arr[1] = j;
					System.out.println(arr[1]);
					return arr;
				}
			}
		}

		return arr;

	}
}
