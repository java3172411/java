class Demo{
	
	int x;

	Demo(int x){
		super();		
		this.x = x;
		System.out.println("In Demo(int x)");
	}
}

class Main{
	
	public static void main(String [] args){
	
		Demo obj = new Demo(10);
		System.out.println("x = "+obj.x);
	}
}
