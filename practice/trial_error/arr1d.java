import java.util.Scanner;
import java.util.Arrays;

class DemoArray{
	public static void main(String [] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of array : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter "+size+ " elements in the array : ");

		for(int x = 0; x<size; x++){
			arr[x] = sc.nextInt();
		}

		System.out.println("Elements in array are: ");
		for(int z = 0; z<size; z++){
			System.out.print(arr[z]+" ");
		}
		System.out.println();
		System.out.println("Length : "+arr.length);
		
		System.out.println("Array : "+Arrays.toString(arr));

	}
}
