import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;

class Demo{
    public static void main(String[] args)throws IOException{
		System.out.println("Enter anything(taking input using br) : ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		br.readLine();	   

		System.out.print("BufferedReader obj : ");
        System.out.println(br); //toString from Object class is called which gets class, class name and hashCode of the object in hex

	char arr[][] = {{1,2,3},{4,5},{7}};
	System.out.println("\nPrinting array using regular for : ");
	for(int r=0; r<arr.length;r++){
		for(int c=0; c<arr[r].length; c++){
			System.out.print(arr[r][c]+" ");		
			System.out.print((int)arr[r][c]+" ");		
		}
		System.out.println();
	}


	System.out.println("\nPrinting array using for each : ");
	for(char [] r : arr){
		for(int c : r){
			System.out.print(c+" ");
		}
		System.out.println();
	}
	
	System.out.println("\nPrinting array using Arrays class's toString : ");
	System.out.println(Arrays.toString(arr));
	System.out.println(Arrays.toString(arr[0]));
	System.out.println(Arrays.toString(arr[1]));
	System.out.println(Arrays.toString(arr[2]));
    }
}
