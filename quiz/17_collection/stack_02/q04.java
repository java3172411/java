import java.util.*;

class StackDemo{

        public static void main(String[] args){

                Stack<Integer>stack =  new Stack<>();

                stack.push(100);
                stack.push(200);
                stack.push(300);
                stack.push(400);    

                //400,300,200,100

                Stack<Integer>stack1 = new Stack<>();

                stack1.push(stack.pop());
                stack1.push(stack.pop());
                stack1.push(stack.pop());

                System.out.println(stack1.peek());
                //200,300,400
        }
}