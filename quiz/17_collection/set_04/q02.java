import java.util.*;

class Demo{
    public static void main(String [] args){
        HashSet<Integer> hs = new HashSet<>(5);

        hs.add(100);
        hs.add(99);
        hs.add(98);
        hs.add(97);
        hs.add(96);

        HashSet<Integer> set = new HashSet<>(hs.size());

        set = (HashSet)hs.clone();
        hs.clear();
        Iterator itr = hs.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}