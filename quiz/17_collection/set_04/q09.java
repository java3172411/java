import java.util.*;

class Demo{
    public static void main(String[] args) {
        LinkedHashSet<String> lhs1 = new LinkedHashSet<>();
        lhs1.add("Hi");
        LinkedHashSet<StringBuffer> lhs2 = new LinkedHashSet<>();
        lhs2.add(new StringBuffer("Hi"));

        System.out.println(lhs1.equals(lhs2));
    }
}