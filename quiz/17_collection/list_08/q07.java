import java.util.*;

class User{
    private String name;

    public User(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}

class Demo{
    public static void main(String [] args){
        ArrayList<User> ll = new ArrayList<>();

        ll.add(new User("Sun"));
        ll.add(new User("China"));
        ll.add(new User("Bhutan"));
        ll.add(new User("Indonesia"));

        String str = new String("sun");

        System.out.println(str.equals(ll.get(0).toString()));
    }
}