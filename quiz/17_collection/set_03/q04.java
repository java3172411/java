import java.util.*;

class Demo{
    public static void main(String [] args){
        HashSet hs = new HashSet(5);

        hs.add(100);
        hs.add('p');
        hs.add('a');
        hs.add("Hello");
        hs.add(200);

        // System.out.println(hs);

        // for(Object var1 : hs)  //var datatype is valid from java 10!
        //     System.out.println(var1);

        System.out.println(hs.size());
        hs.clear();
        hs.remove(100);
        System.out.println(hs);
    }
}   