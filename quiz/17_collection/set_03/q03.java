import java.util.*;

class Demo{
    public static void main(String [] args){
        HashSet hs = new HashSet();

        hs.add("Programmer");
        hs.add("Coder");
        hs.add("Developer");
        hs.add('Z');

        for(var var1 : hs)  //var datatype is valid from java 10!
            System.out.println(var1);
    }
}   