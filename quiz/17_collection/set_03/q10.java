import java.util.*;

class Demo {
    public static void main(String[] args) {
        HashSet hs = new HashSet(5);

        hs.add(100);
        hs.add('p');
        hs.add('a');
        hs.add("Hello");
        hs.add(200);

        System.out.println("Size before clear: " + hs.size());  // Output: 5
        hs.clear();
        System.out.println("Size after clear: " + hs.size());   // Output: 0

        boolean isRemoved = hs.remove(100); //If the element is not found, the method silently returns false instead of throwing an exception.
        System.out.println("Was 100 removed? " + isRemoved);    // Output: false

        System.out.println(hs);  // Output: []
    }
}
