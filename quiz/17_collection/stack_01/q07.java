import java.util.*;

class StackDemo{

        public static void main(String[] args){

                Stack<String>stack =  new Stack<>();

                stack.push("Java");
                stack.push("CPP");
                stack.push("Python");
                stack.push("C");
                stack.push("C#");

                stack.pop(5);
                System.out.println(stack);
        }
}