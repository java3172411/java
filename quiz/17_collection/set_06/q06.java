import java.util.*;

class Demo {

    public static void main(String[] args) {

        NavigableSet<String> strSet = new TreeSet<>();

        strSet.add("C");
        strSet.add("CPP");
        strSet.add("Java");
        strSet.add("DSA");

        try {
            //strSet.removeAll(strSet);
            strSet.add(null);
            //System.out.println(strSet);
        } catch (NullPointerException npe) {
            System.err.println("Caught NullPointerException: " + npe.getMessage());
        }
    }
}