import java.util.*;

class Demo{
    public static void main(String [] args){

        NavigableSet<String>  set = new TreeSet<>();

        set.add("alphabet");
        set.add("apple");
        set.add("amazon");
        set.add("Google");
        set.add("Meta");
        set.add("Netflix");

        NavigableSet set1 = set.descendingSet();

        System.out.println(set1.first());
        System.out.println(set.last());


    }
}