import java.util.*;

class User{
        private String name;
        private int rollNo;

        public User(String name, int rollNum){
                this.name = name;
                this.rollNo = rollNum;
        }
        public String toString(){
                return name+" "+rollNo;
        }
}
class TreeSetDemo{


        public static void main(String[] args){

                SortedSet<User> sortedSet = new TreeSet<>();
                sortedSet.add(new User("Z",1));
                sortedSet.add(new User("G",2));
                sortedSet.add(new User("L",3));
                sortedSet.add(new User("A",4));

                TreeSet<User> ts = new TreeSet<>();
                System.out.println(ts);
        }
}