import java.util.*;

class User{
        private String name;
        private int rollNo;

        public User(String name, int rollNum){
                this.name = name;
                this.rollNo = rollNum;
        }
        public String toString(){
                return name+" "+rollNo;
        }
}
class TreeSetDemo{


        public static void main(String[] args){

                SortedSet<User> sortedSet = new TreeSet<>();
                sortedSet.add("A-User",1);
                sortedSet.add("D-User",2);
                sortedSet.add("H-User",3);
                sortedSet.add("I-User",4);

                TreeSet<User> ts = new TreeSet<>();
                System.out.println(ts);
        }
}