import java.util.*;

class ArrayListDemo{
	
	public static void main(String [] args){
		
		ArrayList<Integer> list = new ArrayList<Integer>(5);

		for(int i=0; i<5; i++){
			
			list.add(i);
		}

		for(Integer x : list)
			System.out.println(x);
	}
}
