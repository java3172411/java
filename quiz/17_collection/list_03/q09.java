import java.util.*;

class ArrayListDemo{
	
	public static void main(String [] args){
		
		ArrayList<Integer> list = new ArrayList<Integer>(5);

		list.add(9);
        list.add(2);
        list.add(8);
        list.add(-2);
        list.add(0);

        System.out.println(list.get(1));
        System.out.println(list.get(5));
	}
}

