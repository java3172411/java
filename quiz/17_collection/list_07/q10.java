import java.util.LinkedList;

class Demo{
    public static void main(String [] args){
        LinkedList list = new LinkedList();

        list.add("ABC");
        list.add("140");
        list.add(8.9f);

        System.out.println(list.remove("140"));
        System.out.println(list.removeFirstOccurrence("ABC"));
        System.out.println(list);
    }
}