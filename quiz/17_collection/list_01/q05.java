import java.util.*;

class Core2Web{
	
	public static void main(String [] args){
		
		NavigableSet<String> list = new TreeSet<>();

		list.add("Core2Web");
		list.add("Biencaps");
		list.add("Incubators");

		Iterator<String> itr = list.iterator();

		while(itr.hasNext()){
			System.out.println(itr.next());
		}
	}
}
