import java.util.*;

class Demo{
	
	public static void main(String [] args){
		
		LinkedList<String> list = new LinkedList<>();

		list.add("Good");
		list.add("Better");
		list.add("Nice");

		System.out.println("list::"+list);

		HashSet<String> set = new HashSet<String>(list);

		System.out.println("HashSet::"+set);
	}
}
