import java.util.Scanner;

class p10{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Math = ");
		int math = sc.nextInt();

		System.out.print("English = ");
		int eng = sc.nextInt();

		System.out.print("Science = ");
		int sci = sc.nextInt();

		System.out.print("Marathi = ");
		int mar = sc.nextInt();

		int sum = math+eng+sci+mar;
		if(sum>400){
			
			System.out.println("Marks of a subject cannot be above 100!");
		}else{
			
			System.out.println("Total score = " + sum);
		}
	}
}
