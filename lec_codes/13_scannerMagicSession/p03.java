import java.util.*;

class p03{
	
	public static void main(String[] args){
		
		System.out.println(" num = ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		if(num%8==0){
			
			System.out.println(num + " is divisible by 8");
		}else{
			
			System.out.println(num + " is not divisible by 8");
		}
	}
}
