import java.util.*;

class p05{
	
	public static void main(String[] args){
		
		System.out.println("num = ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		if(num%16 == 0){
			
			System.out.println(num + " is divisible by 16");
		}else{
			
			System.out.println(num + " is not divisible by 16");
		}
	}
}
