class Demo{
	
	private static int x = 10;
	static private int y = 20;

	void fun(){
		
		System.out.println("In fun");	
		System.out.println(x);
		System.out.println(y);
	}

}

class Client{
	
	public static void main(String[] args){
		
		Demo obj = new Demo();
		System.out.println(obj.x);
		System.out.println(obj.y);
		System.out.println(Demo.x);
		System.out.println(Demo.y);

	}
}
