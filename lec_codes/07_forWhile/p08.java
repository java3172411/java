class p08_LoopDemo{
	
	public static void main(String[] args){
		
		for(int x = 1; x<=5; x++){
			
			System.out.println("Hi : " + x);
		}

		int i = 1;
		while(i<=5){
			
			System.out.println("Hi : " + i);
			i++;
		}
	}
}
