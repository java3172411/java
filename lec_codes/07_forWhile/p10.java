class p10_LoopDemo{
	
	public static void main(String[] args){
		
		for(int i = 1; i<=5; i++){
			
			System.out.println("Hi : " + i);
		}

		System.out.println();

		int i = 1;
		while(i<=5){
			
			System.out.println("Hi : " + i);
			i++;
		}
	}
}
