class Parent1{
	
	Parent1(){
		
		System.out.println("Parent1");
	}
}
class Parent2{
	
	Parent2(){
		
		System.out.println("Parent2");
	}
}
class Child extends Parent1, Parent2{
	
	Child(){
		
		System.out.println("Child");
	}
}
