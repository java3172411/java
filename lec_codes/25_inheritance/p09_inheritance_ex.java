class Facebook{
	
	void companyOwner(){
		
		System.out.println("Company owner is Mark Zukerburg");
	}

	void features(){
		
		System.out.println("Features : post pictures, comments, chat, create groups");
	}
}

class Instagram extends Facebook{
	
	void extraFeatures(){
		
		System.out.println("Reels, stories, channels");
	}
}

class Exe{
	
	public static void main(String [] args){
		
		Instagram ig = new Instagram();
		ig.companyOwner();
		ig.features();
		ig.extraFeatures();
	}
}
