class p07_Core2web{
	
	public static void main(){	//Error: Main method not found in class p07_Core2web, please define the main method as:
					//   public static void main(String[] args)
					//   or a JavaFX application class must extend javafx.application.Application
		
		System.out.println("Start Main");
		System.out.println("Core2Web family");
		System.out.println("End Main");
	}
}
