import java.util.*;

class ListMethods{
	
	public static void main(String [] args){
		
		List al = new ArrayList();
		
		al.add(10);
		al.add(20);
		al.add("Shashi");
		al.add("Kanha");
		al.add(30.5);
		System.out.println(al);

		al.add(3,"Kanha");
		al.add(5,"Ashish");
		al.add(7,"Sam");
		System.out.println(al);

		List ll1 = new LinkedList();

		ll1.add("Sam");
		ll1.add(10);
		ll1.add(20);
		System.out.println(ll1);

		System.out.println(al.containsAll(ll1));

	}
}
