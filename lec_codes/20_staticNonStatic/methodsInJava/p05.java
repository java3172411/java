class Demo{
	
	static void fun(){
	
		System.out.println("In static method - fun");
	}

	public static void main(String[] args){
	
		//first
		Demo obj = new Demo();
		obj.fun();

		//second
		fun();

		//third
		Demo.fun();
	}
}

	
