import java.util.*;

class MapMethods{
	
	public static void main(String[] args){
	
		Map m = new HashMap();

		//put(K,V)
		m.put("A", "Microsoft");
		m.put("D", "Amazon");
		m.put("B", "Google");
		m.put("C", "Apple");
		m.put("E", "Google");
		System.out.println("map1 : "+m);
		
		Map m2 = new HashMap();

		//put(K,V)
		m2.put("A", "Microsoft");
		m2.put("D", "Amazon");
		m2.put("B", "Google");
		m2.put("C", "Apple");
		m2.put("E", "Google");
		System.out.println("map2 : "+m2);

		//equals(java.lang.Object)
		System.out.println(m.equals(m2));

		//putIfAbsent(K,V)
		System.out.println(m.putIfAbsent("S", "Varian"));
		System.out.println(m);
		System.out.println(m.putIfAbsent("S", "Varian"));
		System.out.println(m);
		

		//getOrDefault
		System.out.println(m.getOrDefault("S","Varian"));
		System.out.println(m);
		System.out.println(m.getOrDefault("G","GE"));
		System.out.println(m);

	
		HashMap hm = new HashMap();

		//put(K,V)
		hm.put("A", "Microsoft");
		hm.put("D", "Amazon");
		hm.put("B", "Google");
		hm.put("C", "Apple");
		hm.put("E", "Google");
		
		System.out.println("HashMap : "+hm);

		System.out.println("capacity : "+hm.capacity());
		System.out.println("entrySet : "+hm.entrySet());
		System.out.println("keySet : "+hm.keySet());
		System.out.println("values : "+hm.values());

			
	
	}
	
}
