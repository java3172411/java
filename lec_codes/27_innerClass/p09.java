class Outer{
	
	Outer(){
		
		System.out.println("Outer Constructor");
	}

	int x = 10;
	int z = 90;
	static int y = 20;

	void fun(){
	
		System.out.println("In fun");
	}

	class Inner{
		
		int x = 30;
		Inner(int x){
			
			System.out.println("Inner Constructor");
			System.out.println(this.x);	//30, Inner class's x
			System.out.println(y);		//20, Static var of Outer
			System.out.println(x);		//40, Arg passed to Inner constructor
			System.out.println(z);		//90

			//PRINT : 10, Inner's global x
			//System.out.println(obj.x);	// error at obj: cannot find symbol
			//System.out.println(this$0.x); //error at this$0 : cannot find symbol
			//System.out.println(Outer.x);	//error: NON STATIC VARIABLE X CANNOT BE CONTEXTED FROM
							//STATIC CONTEXT
			//System.out.println(Inner.x);  //error : NON STATIC VARIABLE X CANNOT BE CONTEXTED FROM
							//STATIC CONTEXT

			Outer obj = new Outer();
			System.out.println(obj.x);
			
			fun();

		}
	}
	public static void main(String [] args){
		
		Inner obj = new Outer().new Inner(40);
	}
};
