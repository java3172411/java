import java.util.*;

class p10_charInScanner{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter your name: ");
		String name = sc.next();

		System.out.print("Enter society name: ");
		String socName = sc.next();

		System.out.print("Enter wing: ");
		//char wing = sc.next();	//error: incompatible types: String cannot be converted to char. To solve this error we use: .charAt(index) as string is nothing but a array of char we use .charAt to read a single character at a particular index. and as the Scanner automatically clears the buffer the problem of '\n' remaining in the buffer doesn't occur
		char wing = sc.next().charAt(0);

		System.out.print("Enter flat number: ");
		int flatNo = sc.nextInt();
	       
		System.out.println();
		System.out.println("Name : " + name);
		System.out.println("Society Name : " + socName);
		System.out.println("Wing : " + wing);
		System.out.println("Flat No. : " + flatNo);
	
	}
}
