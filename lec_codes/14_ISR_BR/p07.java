import java.io.*;

class p07_brSkip{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter your name: ");
		String name = br.readLine();

		System.out.print("Enter wing: ");
		char wing = (char)br.read();

		System.out.print("Enter society name: ");
		String socName = br.readLine();
		

		System.out.println();
		System.out.println("Name : " + name);
		System.out.println("Wing : " + wing);
		System.out.println("Society Name : " + socName); //as the readLine function finds previous '\n' in the buffer it reads that '\n' as complete line and hence doesn't wait for the user to enter any input
			
	}
}
