import java.util.Scanner;

class p01_ScannerDemo{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter name: ");
		String name = sc.next();

		System.out.print("Enter college name: ");
		String clgName = sc.next();

		System.out.print("Enter Student id: ");
		int stdId = sc.nextInt();

		System.out.print("Enter CGPA: ");
		float cgpa = sc.nextFloat();

		System.out.println("Student name: " + name);
		System.out.println("College name: " + clgName);
		System.out.println("Student id: " + stdId);
		System.out.println("CGPA: " + cgpa);
	}
}
