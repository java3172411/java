import java.io.*;

class p03_InputDemo{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Company name: ");
		String compName = br.readLine();

		System.out.print("Enter Employee name: ");
		String empName = br.readLine();

		System.out.print("Enter empId: ");
		int empId = Integer.parseInt(br.readLine());

		System.out.print("Enter Salary: ");
		double sal = Double.parseDouble(br.readLine());

		System.out.println("Company Name: " + compName);
		System.out.println("Employee Name: " + empName);
		System.out.println("Employee Salary: " + sal);
		System.out.println("Employee id: " + empId);
	}
}
