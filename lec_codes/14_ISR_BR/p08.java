import java.io.*;

class p08_brSkip{
	
	public static void main(String[] args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter your name: ");
		String name = br.readLine();

		System.out.print("Enter society name: ");
		String socName = br.readLine();

		System.out.print("Enter wing: ");
		char wing = (char)br.read();

		br.skip(1); //here br.skip skips that '\n' in the 1st index(indices similar to an array)

		System.out.print("Enter flat number: ");
		int flatNo = Integer.parseInt(br.readLine()); //Enter flat number: Exception in thread "main" java.lang.NumberFormatException: For input string: ""  # this exception occurs as the br.read function reads only one character from the buffer as a result the '\n' from the previous char input is still in the buffer and the readLine function tried to read '\n' and then Interger.parseInt tries to convert it to int which is not possible hence the nUMBERfORMATeXCEPTION occurs, TO_SOLVE this error we use br.skip

		System.out.println();
		System.out.println("Name : " + name);
		System.out.println("Society Name : " + socName);
		System.out.println("Wing : " + wing);
		System.out.println("Flat No. : " + flatNo);
	
	}
}
