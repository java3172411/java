class p04_InputDemo{
	
	void methodFun(){
	
		System.out.println("In fun method");
	}

	void methodRun(){
		
		System.out.println("In run method");
	}

	void methodGun(){
		
		System.out.println("In gun method");
	}

	public static void main(String[] args){
		
		System.out.println("In main method");

		p04_InputDemo obj = new p04_InputDemo();

		obj.methodFun();
		obj.methodRun();
		obj.methodGun();
	}

}
