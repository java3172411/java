class p03_InputDemo{
	
	void fun(){
		
		System.out.println("In fun method.\nFun is non-static method i.e. instance method, hence requires an object in order to be called.\nAll the 'Non static(instance)' methods get memory at the 'Heap' in JVM.\n");
	}

	static void run(){
		
		System.out.println("In run method.\n Run is a static method, it does not require and object and can be called directly.\nAll the 'Static methods' in a program get memory at the 'Method area' in JVM\n");
	}

	public static void main(String[] args){
		
		System.out.println("In main method\nAs 'main' is also a 'static' method, it gets memory in the 'Memory area'\n");
		System.out.println("calling method run: run(); or p03_InputDemo.run();");
		run();		//p03_InputDemo.run()

		System.out.println("As fun is an instance method it cannot be called like run.\nHence an object is created as:\n->p03_InputDemo obj = new p03_InputDemo();\nand then the method is called over the object as:\n->obj.fun();\n");

		p03_InputDemo obj = new p03_InputDemo();
		obj.fun();
	}
}
