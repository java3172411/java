class p06_StaticDemo{
	
	int x = 30;
	static int y = 40;

	void fun(){
		
		System.out.println("In method fun");
	}

	static void run(){
		
		System.out.println("In run");
	}

	public static void main(String[] args){
		
		p06_StaticDemo obj = new p06_StaticDemo();

		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.fun();
		obj.run();
	}
}
