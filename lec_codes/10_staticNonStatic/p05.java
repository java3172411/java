class p05_StaticDemo{
	
	int x = 10;
	static int y = 20;

	public static void main(String[] args){
		
		//System.out.println(x);    error: non-static variable x cannot be referenced from a static context
		System.out.println(y);

		p05_StaticDemo obj  = new p05_StaticDemo();

		System.out.println(obj.x);
		System.out.println(obj.y);
	}
}
