/*
 A B C D
 a b c d
 A B C D
 a b c d
 */

class p16{
	
	public static void main(String[] args){
		
		for(int i = 1; i<=4; i++){
			
			char A = 'A';
			char a = 'a';

			for(int j = 1; j<=4; j++){
				
				if(i%2==1){
					
					System.out.print(A + " ");
					A++;
				}else{
					
					System.out.print(a + " ");
					a++;
				}
			}

			System.out.println();
		}
	}
}
