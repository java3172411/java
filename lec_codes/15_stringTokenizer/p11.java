//code37
import java.util.*;

class p11_InputDemo{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter your name: ");
        String name = sc.nextLine();
        
        System.out.print("Enter your Society name: ");
        String socName = sc.nextLine();

        System.out.print("Enter your wing: ");
        char wing = sc.next().charAt(0);

        System.out.print("Enter your flatNo: ");
        int flatNo = sc.nextInt();

        System.out.println();
        System.out.println("Name: "+name);
        System.out.println("Society Name: "+socName);
        System.out.println("Wing: "+wing);
        System.out.println("flatNo: "+flatNo);
    }
}
