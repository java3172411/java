//code28
import java.io.*;

class p02_InputDemo{
    public static void main(String[] args)throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter your name: ");
        String name = br.readLine();
        br.close();
        System.out.print("Enter your Society name: ");
        String socName = br.readLine();

        System.out.print("Enter your wing: ");
        String wing = br.readLine();

        System.out.print("Enter your flatNo: ");
        String flatNo = br.readLine();

        System.out.println();
        System.out.println(name);
        System.out.println(socName);
        System.out.println(wing);
        System.out.println(flatNo);
    }
}
