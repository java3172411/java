//code36
import java.io.*;

class p10a_InputDemo{
    public static void main(String[] args)throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter your name: ");
        String name = br.readLine();
        
        System.out.print("Enter your Society name: ");
        String socName = br.readLine();

        System.out.print("Enter your wing: ");
        char wing = (char)br.read();

        System.out.print("Enter your flatNo: ");
        String flatNo = br.readLine();

        System.out.println();
        System.out.println("Name: "+name);
        System.out.println("Society Name: "+socName);
        System.out.println("Wing: "+wing);
        System.out.println("flatNo: "+flatNo);
    }
}
