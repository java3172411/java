/*
 4 3 2 1
 4 3 2 1
 4 3 2 1
 4 3 2 1
 */

class p14{
	
	public static void main(String[] args){
		
		int row=4;
		for(int i=1; i<=row; i++){
			
			int x=row;
			for(int j=1; j<=row; j++){
				
				System.out.print(x-- + " ");
			}
			System.out.println();
		}
	}
}
