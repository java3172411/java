//A 2 C 4 E 6 G 8 I

class p08{
	
	public static void main(String[] args){
		
		int num = 1;			//fixed line
		num+=64;
		for(int i=1; i<=9; i++){
			
			if(i%2 == 1){
			
				System.out.print((char)num + " ");
			}else{
				
				System.out.print(i + " ");
			}
			num++;
		}
		System.out.println();
	}
}
