import java.util.*;

class p04_SingleLineIp{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter player info: ");
		String info = sc.next();

		StringTokenizer st = new StringTokenizer(info, ","); //class in util package

		String str1 = st.nextToken(); //name
		String str2 = st.nextToken();    //jerseyNo
		String str3 = st.nextToken();  //avg
		String str4 = st.nextToken();   //grade
					      
		System.out.println();
		System.out.println("player name : "+str1);
		System.out.println("jerseyNo : "+str2);		//MSD 7
					//Exception in thread "main" java.util.NoSuchElementException
					//occurs when wrong delimeter is used or less number of input given
		System.out.println("player avg : "+str3);
		System.out.println("player grade : "+str4);

	}
}
