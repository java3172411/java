import java.util.*;

class p05_SingleLineIp{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter player info: ");
		String info = sc.next();

		StringTokenizer st = new StringTokenizer(info, ",!#$&*"); //class in util package

		while(st.hasMoreTokens()){
			
			System.out.println(st.nextToken());
		}
	}
}
