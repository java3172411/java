import java.util.*;

class p03_SingleLineIp{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter player info: ");
		String info = sc.next();

		StringTokenizer st = new StringTokenizer(info, ",");

		String str1 = st.nextToken(); //name
		int str2 = st.nextToken();    //jerseyNo
		float str3 = st.nextToken();  //avg
		char str4 = st.nextToken();   //grade
					      
		System.out.println();
		System.out.println("player name : "+str1);
		System.out.println("jerseyNo : "+str2);
		System.out.println("player avg : "+str3);
		System.out.println("player grade : "+str4);

	}
}
