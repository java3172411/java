import java.util.*;

class Demo{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		int empId[] = new int [10];
		
		int num = 10;

		for(int i = 0;i<=9;i++){
			
			empId[i] = num;
			num+=10;
			System.out.print(empId[i] + " ");
		}
		System.out.println();

		String langs[] = new String[10];
		System.out.println("Enter in one line any 5 programming languages you know by giving ' ' between them: ");

		String lang = sc.next();
		StringTokenizer stk = new StringTokenizer(lang," ");
	
		for(int i=0; i<langs.length; i++){
		
			while(stk.hasMoreTokens() && i<langs.length){
				langs[i] = stk.nextToken();
				System.out.print("lang "+i+" : "+langs[i]);
				i=(langs.length)+1;
			}
		}

		System.out.println();
		
	}
}
